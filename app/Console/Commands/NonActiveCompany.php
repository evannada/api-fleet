<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Company;
use Carbon\Carbon;

class NonActiveCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'non-active-company';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set Status Non Active Company';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $company = Company::where('due_date', '<=',date('Y-m-d'))->get();

        if (count($company)) {
            $id = [];
            foreach ($company as $row) {
                array_push($id, $row->id);
            }
            //update status compnay to non-active
            Company::whereIn('id', $id)->update(['status' => 0]);
        }
    }
}
