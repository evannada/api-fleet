<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Payment;
use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use App\Mail\NotificationPayment;
use Illuminate\Support\Facades\Mail;
use App\Models\Countercode;

class SendInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-invoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Invoice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $payment = Payment::where('no_payment', 'like', 'DIG%')->where('date_sendinv', Carbon::now()->toDateString())->get();

        if (count($payment)) {
            foreach ($payment as $row) {
                $company = Company::whereId($row->company_id)->first();

                $unit_price = 79000;
                //calculate total price per 3 months
                $total_price = $company->number_user * $unit_price * 3;

                //generate no payment
                $no_payment = $this->countercodeDIG($company->id);

                Payment::create([
                    'company_id' => $company->id,
                    'no_payment' => $no_payment,
                    'date_order' => date("Y-m-d"),
                    'via' => null,
                    'number_user' => $company->number_user,
                    'unit_price' => $unit_price,
                    'total_price' => $total_price,
                    'date_paid' => null,
                    'status' => 0
                ]);

                //make signed url to route get /order-payment on web.php
                $url_order_payment = URL::signedRoute('order.payment', [ 'no_payment' => $no_payment ]);
                
                $details = [
                    'to' => $company->email,
                    'owner_name' => $company->owner_name,
                    'address' => $company->address,
                    'message' => 'Berikut adalah link untuk pembayaran penggunaan Digkontrol 3 bulan berikutnya. ',
                    'phone' => $company->phone,
                    'npwp' => $company->npwp,
                    'no_payment' => $no_payment,
                    'url_order_payment' => $url_order_payment
                ];

                //send email order payment
                Mail::to($company->email)->send(new NotificationPayment($details));
            
            }
        }
    }

    public function countercodeDIG($company_id)
    {
        $company_id = str_pad($company_id,3,0,STR_PAD_LEFT);
        $countercode = Countercode::whereType('DIG')
                    ->whereFor($company_id)
                    ->whereYear('max_date','=', date('Y'))->first();

        if(!$countercode){
            $insert = Countercode::insert([
                    'type' => 'DIG',
                    'for'=>$company_id,
                    'lastcounter'=> 1,
                    'max_date' => date('Y').'-12-31',
                ]);

            $nxt = 1;
        }
        else{ 
            $nxt = $countercode->lastcounter + 1; 
            $update = Countercode::where('id',$countercode->id)->update(['lastcounter'=>$nxt]);
        }

        $nxt = str_pad($nxt,4,0,STR_PAD_LEFT);
        $code = 'DIG'.'/'.$company_id.'/'.date('m').date('y').'/'.$nxt;
        
        return $code;
    }
}
