<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Models\Equipment;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NotificationSimToDriver;
use App\Notifications\NotificationSimToAdmin;

class SimNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sim-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sim Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $equipment_7 = Equipment::whereRaw('CURDATE() = DATE_SUB(sim_expired, INTERVAL 7 DAY)')->get();
        $equipment_6 = Equipment::whereRaw('CURDATE() = DATE_SUB(sim_expired, INTERVAL 6 DAY)')->get();
        $equipment_5 = Equipment::whereRaw('CURDATE() = DATE_SUB(sim_expired, INTERVAL 5 DAY)')->get();
        $equipment_4 = Equipment::whereRaw('CURDATE() = DATE_SUB(sim_expired, INTERVAL 4 DAY)')->get();
        $equipment_3 = Equipment::whereRaw('CURDATE() = DATE_SUB(sim_expired, INTERVAL 3 DAY)')->get();
        $equipment_2 = Equipment::whereRaw('CURDATE() = DATE_SUB(sim_expired, INTERVAL 2 DAY)')->get();
        $equipment_1 = Equipment::whereRaw('CURDATE() = DATE_SUB(sim_expired, INTERVAL 1 DAY)')->get();

        if (count($equipment_7)) {
            foreach ($equipment_7 as $row) {
                $userDriver = User::with('equipment.simtype')->whereId($row->user_id)->first();
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $sim_expired = date('d F Y', strtotime($row->sim_expired));

                $details = [
                    'userDriver' => $userDriver,
                    'sim_expired' => $sim_expired
                ];

                Notification::send($userDriver, new NotificationSimToDriver($details));
                Notification::send($userAdmin, new NotificationSimToAdmin($details));
            
            }
        }

        if (count($equipment_6)) {
            foreach ($equipment_6 as $row) {
                $userDriver = User::with('equipment.simtype')->whereId($row->user_id)->first();
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $sim_expired = date('d F Y', strtotime($row->sim_expired));

                $details = [
                    'userDriver' => $userDriver,
                    'sim_expired' => $sim_expired
                ];

                Notification::send($userDriver, new NotificationSimToDriver($details));
                Notification::send($userAdmin, new NotificationSimToAdmin($details));
            
            }
        }

        if (count($equipment_5)) {
            foreach ($equipment_5 as $row) {
                $userDriver = User::with('equipment.simtype')->whereId($row->user_id)->first();
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $sim_expired = date('d F Y', strtotime($row->sim_expired));

                $details = [
                    'userDriver' => $userDriver,
                    'sim_expired' => $sim_expired
                ];

                Notification::send($userDriver, new NotificationSimToDriver($details));
                Notification::send($userAdmin, new NotificationSimToAdmin($details));
            
            }
        }

        if (count($equipment_4)) {
            foreach ($equipment_4 as $row) {
                $userDriver = User::with('equipment.simtype')->whereId($row->user_id)->first();
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $sim_expired = date('d F Y', strtotime($row->sim_expired));

                $details = [
                    'userDriver' => $userDriver,
                    'sim_expired' => $sim_expired
                ];

                Notification::send($userDriver, new NotificationSimToDriver($details));
                Notification::send($userAdmin, new NotificationSimToAdmin($details));
            
            }
        }

        if (count($equipment_3)) {
            foreach ($equipment_3 as $row) {
                $userDriver = User::with('equipment.simtype')->whereId($row->user_id)->first();
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $sim_expired = date('d F Y', strtotime($row->sim_expired));

                $details = [
                    'userDriver' => $userDriver,
                    'sim_expired' => $sim_expired
                ];

                Notification::send($userDriver, new NotificationSimToDriver($details));
                Notification::send($userAdmin, new NotificationSimToAdmin($details));
            
            }
        }

        if (count($equipment_2)) {
            foreach ($equipment_2 as $row) {
                $userDriver = User::with('equipment.simtype')->whereId($row->user_id)->first();
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $sim_expired = date('d F Y', strtotime($row->sim_expired));

                $details = [
                    'userDriver' => $userDriver,
                    'sim_expired' => $sim_expired
                ];

                Notification::send($userDriver, new NotificationSimToDriver($details));
                Notification::send($userAdmin, new NotificationSimToAdmin($details));
            
            }
        }

        if (count($equipment_1)) {
            foreach ($equipment_1 as $row) {
                $userDriver = User::with('equipment.simtype')->whereId($row->user_id)->first();
                $userAdmin = User::whereCompany_id($row->company_id)
                            ->whereRole_id(2)->get();

                $sim_expired = date('d F Y', strtotime($row->sim_expired));

                $details = [
                    'userDriver' => $userDriver,
                    'sim_expired' => $sim_expired
                ];

                Notification::send($userDriver, new NotificationSimToDriver($details));
                Notification::send($userAdmin, new NotificationSimToAdmin($details));
            
            }
        }
    }
}
