<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\KirNotification::class,
        \App\Console\Commands\SimNotification::class,
        \App\Console\Commands\StnkNotification::class,
        \App\Console\Commands\InsuranceNotification::class,
        \App\Console\Commands\ReminderNotification::class,
        \App\Console\Commands\SendInvoice::class,
        \App\Console\Commands\NonActiveCompany::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('kir-notification')->timezone('Asia/Jakarta')->daily();
        $schedule->command('sim-notification')->timezone('Asia/Jakarta')->daily();
        $schedule->command('stnk-notification')->timezone('Asia/Jakarta')->daily();
        $schedule->command('insurance-notification')->timezone('Asia/Jakarta')->daily();
        $schedule->command('reminder-notification')->timezone('Asia/Jakarta')->daily();
        $schedule->command('send-invoice')->timezone('Asia/Jakarta')->daily();
        $schedule->command('non-active-company')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
