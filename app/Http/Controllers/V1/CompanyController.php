<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Mail\NotificationPaymentAddUser;
use App\Models\Company;
use App\Models\Countercode;
use App\Models\Payment;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Veritrans_Config;

class CompanyController extends Controller
{
    /**
     * Make request global.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Class constructor.
     *
     * @param \Illuminate\Http\Request $request User Request
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        // Set midtrans configuration
        Veritrans_Config::$serverKey = config('services.midtrans.serverKey');
        Veritrans_Config::$isProduction = config('services.midtrans.isProduction');
        Veritrans_Config::$isSanitized = config('services.midtrans.isSanitized');
        Veritrans_Config::$is3ds = config('services.midtrans.is3ds');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'nullable|exists:companies,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Company::query();

        if ($request->has('id')) {
            $query = $query->whereId($request->id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
  
        $company = $query->orderBy('name', 'ASC')->get();
        $response = [
            'status' => 'success',
            'data' => $company
        ];
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'owner_name' => 'required|string',
            'email' => 'required|email',
            'address' => 'nullable|string',
            'phone' => 'required|string',
            'npwp' => 'nullable|string',
            //'file_surat_jalan' => 'nullable|string',
            'file_invoice' => 'nullable|string',

        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $file_surat_jalan = '';

        if ($request->has('file_surat_jalan')) {
            if (preg_match('/^data:image\/(\w+);base64,/', $request->file_surat_jalan)) {
                $file_surat_jalan = substr($request->file_surat_jalan, strpos($request->file_surat_jalan, ",")+1);
                $img_name = time() . '.' . str_random(10) . '.jpg';
                $img_path = public_path() . "/uploads/order/" . $img_name;
                file_put_contents($img_path, base64_decode($img_name));
                $file_surat_jalan = config('global.url'). "/uploads/order/" . $img_name;
            }
        } else {
            $file_surat_jalan = config('global.url'). "/uploads/no_image.jpg";
        }

        $file_invoice = '';

        if ($request->has('file_invoice')) {
            if (preg_match('/^data:image\/(\w+);base64,/', $request->file_invoice)) {
                $file_invoice = substr($request->file_invoice, strpos($request->file_invoice, ",")+1);
                $img_name = time() . '.' . str_random(10) . '.jpg';
                $img_path = public_path() . "/uploads/order/" . $img_name;
                file_put_contents($img_path, base64_decode($img_name));
                $file_invoice = config('global.url'). "/uploads/order/" . $img_name;
            }
        } else {
            $file_invoice = config('global.url'). "/uploads/no_image.jpg";
        }

        $company = Company::create([
                'name' => $request->name,
                'owner_name' => $request->owner_name,
                'email' => $request->email,
                'address' => $request->address,
                'phone' => $request->phone,
                'npwp' => $request->npwp,
                'due_date' => $request->due_date,
                'file_surat_jalan' => $request->file_surat_jalan,
                'file_invoice' => $request->file_invoice
        ]);
        
        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $company
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::findOrFail($id);
        $response = [
            'status' => 'success',
            'data' => $company
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $validator = Validator::make($request->all(), [
        //     'name' => 'required|string',
        //     'owner_name' => 'required|string',
        //     'email' => 'required|email',
        //     'address' => 'nullable|string',
        //     'phone' => 'required|string',
        //     'npwp' => 'nullable|string',
        //     //'file_surat_jalan' => 'nullable|string',
        //     //'file_invoice' => 'nullable|string',
        // ]);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->file_surat_jalan)) {
            $file_surat_jalan = substr($request->file_surat_jalan, strpos($request->file_surat_jalan, ",")+1);
            $file_surat_jalan_name = time() . '.' . str_random(10) . '.jpg'; 
            $file_surat_jalan_path = public_path() . "/uploads/armada/stnk/" . $file_surat_jalan_name;
            file_put_contents($file_surat_jalan_path, base64_decode($file_surat_jalan));
            $file_surat_jalan = config('global.url'). "/uploads/armada/stnk/" . $file_surat_jalan_name;
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->file_invoice)) {
            $file_invoice = substr($request->file_invoice, strpos($request->file_invoice, ",")+1);
            $file_invoice_name = time() . '.' . str_random(10) . '.jpg';
            $file_invoice_path = public_path() . "/uploads/armada/stnk/" . $file_invoice_name;
            file_put_contents($file_invoice_path, base64_decode($file_invoice));
            $file_invoice = config('global.url'). "/uploads/armada/stnk/" . $file_invoice_name;
        }

        // if($validator->fails()){
        //     return response()->json([
        //         'status' => 'error',
        //         'message' => $validator->errors()
        //     ], 400);
        // }
        // dd($file_surat_jalan);
        $request->merge([
        'file_surat_jalan' => $file_surat_jalan,
        'file_invoice' => $file_invoice,

        ]);
        $company = Company::findOrFail($id)->update($request->all());
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => Company::find($id)
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::findOrFail($id)->delete($id);
        
        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }

    public function userAdditional(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'number_user' => 'required|integer|gt:0',
            'company_id' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $company_id = $request->company_id;

        //Todo Price dari masa trial 0, get data dari table trial
        //TODO batas waktu pembayaran gimana ?

        //calculate total price

        $subscription = Subscription::whereRaw('? between start_date and end_date', date('Y-m-d'))->first();
        //get price from subscription
        $unit_price = $subscription->price;

        $total_price = $request->number_user * $unit_price;

        //generate no payment
        $no_payment = $this->countercodeDIG($company_id);

        $payment = Payment::create([
            'company_id' => $company_id,
            'no_payment' => $no_payment,
            'date_order' => date("Y-m-d"),
            'via' => null,
            'number_user' => $request->number_user,
            'unit_price' => $unit_price,
            'total_price' => $total_price,
            'date_paid' => null,
            'status' => 0
        ]);

        if ($payment){
            if ($unit_price == 0) {
                $storeOrderPayment = $this->storeOrderPayment($payment);
            } else {
                $url_order_payment = Url::temporarySignedRoute('order.payment', Carbon::parse(now())->endOfDay(), [ 'no_payment' => $no_payment ]);

                $company = Company::findOrFail($company_id);

                if($company){
                    $details = [
                        'to' => $company->email,
                        'owner_name' => $company->owner_name,
                        'address' => $company->address,
                        'message' => $payment->no_payment,
                        'phone' => $company->phone,
                        'npwp' => $company->npwp,
                        'no_payment' => $no_payment,
                        'url_order_payment' => $url_order_payment
                    ];

                    //send email order payment
                    Mail::to($company->email)->send(new NotificationPaymentAddUser($details));

                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'These company do not match on our records.'
                    ], 400);
                }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Please check your company email to pay.'
                ], 200);
            }
        }
    }

    public function countercodeDIG($company_id)
    {
        $company_id = str_pad($company_id,3,0,STR_PAD_LEFT);
        $countercode = Countercode::whereType('ADD')
            ->whereFor($company_id)
            ->whereYear('max_date','=', date('Y'))->first();

        if(!$countercode){
            $insert = Countercode::insert([
                'type' => 'ADD',
                'for'=>$company_id,
                'lastcounter'=> 1,
                'max_date' => date('Y').'-12-31',
            ]);

            $nxt = 1;
        }
        else{
            $nxt = $countercode->lastcounter + 1;
            $update = Countercode::where('id',$countercode->id)->update(['lastcounter'=>$nxt]);
        }

        $nxt = str_pad($nxt,4,0,STR_PAD_LEFT);
        $code = 'ADD'.'/'.$company_id.'/'.date('m').date('y').'/'.$nxt;

        return $code;
    }

    public function addUserTransaction($company_id)
    {
        $payment = Payment::whereCompanyId($company_id)->where('no_payment', 'like', 'ADD%')->get();

        $response = [
            'status' => 'success',
            'data' => $payment
        ];

        return response()->json($response, 200);
    }

    public function getOneAddUserTransaction($company_id, $id)
    {
        $payment = Payment::whereCompanyId($company_id)->where('no_payment', 'like', 'ADD%')->whereId($id)->get();

        if (count($payment) > 0) {
            $response = [
                'status' => 'success',
                'data' => $payment[0]
            ];
        } else {
            $response = [
                'status' => 'success',
                'data' => []
            ];
        }

        return response()->json($response, 200);
    }

    private function storeOrderPayment($payment)
    {
        $payment->update([
            'via' => null,
            'date_paid' => Carbon::today(),
            'status' => 1,
        ]);

        $company = Company::findOrFail($payment->company_id);
        $company->update([
            'number_user' => $company->number_user + $payment->number_user,
            'max_user' => $company->max_user + $payment->number_user,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Penambahan user berhasil.'
        ], 200);
    }
}
