<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Itemdetail;
use Carbon\Carbon;

class ItemdetailController extends Controller
{
    /**
     * Make request global.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Class constructor.
     *
     * @param \Illuminate\Http\Request $request User Request
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        // $this->request = $request;

        // Set midtrans configuration
        // Veritrans_Config::$serverKey = config('services.midtrans.serverKey');
        // Veritrans_Config::$isProduction = config('services.midtrans.isProduction');
        // Veritrans_Config::$isSanitized = config('services.midtrans.isSanitized');
        // Veritrans_Config::$is3ds = config('services.midtrans.is3ds');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'nullable|exists:orders,id',
            'item_type' => 'nullable|string',
            'item_code' => 'nullable|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Itemdetail::query();

        if ($request->has('id')) {
            $query = $query->whereId($request->id);
        }

        if ($request->has('order_id')) {
            $query = $query->order_id($request->order_id);
        }

        if ($request->has('item_code')) {
            $query = $query->item_code($request->item_code);
        }
  
        $itemDetail = $query->orderBy('id', 'ASC')->get();
        $response = [
            'status' => 'success',
            'data' => $itemDetail
        ];
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'item_type' => 'required|string',
            'item_code' => 'required|string',
            'item_name' => 'required|string',
            'unit' => 'required|string',
            'qty' => 'required|numeric',
            'weight' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $rows = $request->input('rows');
        foreach ($rows as $row)
        {
            $itemDetails[] = new Itemdetail(array(
                'order_id' => $request->input('order_id'),
                'item_type' => $row['item_type'],
                'item_code' => $row['item_code'],
                'item_name' => $row['item_name'],
                'unit'      => $row['unit'],
                'qty'       => $row['qty'],
                'weight'    => $row['weight'],

            ));
        }

        // dd($itemDetails);

        $itemDetail = Itemdetail::insert($itemDetails);
        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $itemDetail
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $itemDetail = Itemdetail::findOrFail($id);
        $response = [
            'status' => 'success',
            'data' => $itemDetail
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'item_type' => 'required|string',
            'item_code' => 'required|string',
            'item_name' => 'required|string',
            'unit' => 'required|string',
            'qty' => 'required|numeric',
            'weight' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $itemDetail = Itemdetail::findOrFail($id)->update($request->all());
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => Itemdetail::find($id)
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemDetail = Itemdetail::findOrFail($id)->delete($id);
        
        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }

}
