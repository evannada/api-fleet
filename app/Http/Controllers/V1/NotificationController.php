<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use App\Notifications\TestingNotification;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'read_at' => 'nullable|boolean',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = auth()->user();

        if ($request->has('read_at') && $request->read_at == true) {
            $user->unreadNotifications->markAsRead();
        }

        $read = $user->readNotifications()->count();
        $unread = $user->unreadNotifications()->count();
        $notifications = $user->notifications()->take(50)->get();

        $response = [
            'status' => 'success',
            'read' => $read,
            'unread' => $unread,
            'data' => $notifications
        ];
        return response()->json($response, 200);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'message' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = auth()->user();
        $details = [
            'title' => $request->title,
            'message' => $request->message
        ];

        Notification::send($user, new TestingNotification($details));

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $details
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        $notifications = $user->notifications()->findOrFail($id);
        $notifications->markAsRead();

        $response = [
            'status' => 'success',
            'data' => $notifications
        ];

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth()->user();
        $notifications = $user->notifications()->findOrFail($id);
        $notifications->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
    
}
