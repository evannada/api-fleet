<?php

namespace App\Http\Controllers\V1;

use App\Models\Odometerhistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OdometerhistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable|exists:armadas,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Odometerhistory::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
  
        $query = $query->orderBy('id', 'DESC');
        $odometerhistory = $query->get();
        $odometerhistory->load('armada');

        $response = [
            'status' => 'success',
            'data' => $odometerhistory
        ];
        return response()->json($response, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
            'km' => 'required|integer',
            'liter' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $odometerhistory = Odometerhistory::create([
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'date' => date("Y-m-d"),
            'km' => $request->km,
            'liter' => $request->liter,
        ]);

        // update odometer to armada
        $odometerhistory->armada()->update([
            'odometer' => $request->km
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $odometerhistory
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $odometerhistory = Odometerhistory::with('armada')
                        ->whereCompany_id($request->company_id)
                        ->findOrFail($id);
        
        $response = [
            'status' => 'success',
            'data' => $odometerhistory
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
            'km' => 'required|integer',
            'liter' => 'present|nullable|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $odometerhistory = Odometerhistory::whereCompany_id($request->company_id)
                        ->whereArmada_id($request->armada_id)
                        ->findOrFail($id);

        $odometerhistory->update([
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'date' => date("Y-m-d"),
            'km' => $request->km,
            'liter' => $request->liter,
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => $odometerhistory
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $odometerhistory = Odometerhistory::whereCompany_id($request->company_id)
                        ->findOrFail($id);
        $odometerhistory->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];

        return response()->json($response, 200);
    }
}
