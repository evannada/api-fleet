<?php

namespace App\Http\Controllers\V1;

use App\Models\Order;
use App\Models\Route;
use App\Models\Countercode;
use App\User;
use App\Models\Budgetdetail;
use App\Models\Itemdetail;
use App\Models\Orderdetailcost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NotificationCloseOrder;
use Carbon\Carbon;
use PDF;
use App\Models\Armada;
use App\Models\Company;
use App\Models\Schedulearmada;
use App\Models\Scheduleuser;
use Carbon\CarbonPeriod;
use DateTime;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable|exists:armadas,id',
            'user_id' => 'nullable|exists:users,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Order::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('user_id')) {
            $query = $query->whereUser_id($request->user_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $query = $query->orderBy('id', 'DESC');
        $order = $query->get();
        $order->load(['customer', 'armada', 'routes','orderdetailcosts.cost','budgetdetails.cost','itemdetails']);
        
        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);
        
    }

    public function getItemCode(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'order_id' => 'present|exists:orders,id',
            'item_code' => 'present|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Itemdetail::query();

        if ($request->has('order_id')) {
            $query = $query->whereOrder_id($request->order_id);
        }

        if ($request->has('item_code')) {
            $query = $query->whereItem_code($request->item_code);
        }
        
        // $query = $query->orderBy('id', 'DESC');
        $order = $query->get();
        // $order->load(['customer', 'armada', 'routes','orderdetailcosts.cost','budgetdetails.cost','itemdetails']);
        
        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);
        
    }

    public function orderStatusProgress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable|exists:armadas,id',
            'user_id' => 'nullable|exists:users,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // Open :
        // 0. Open

        $query = Order::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('user_id')) {
            $query = $query->whereUser_id($request->user_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status_order',[0]);
        $query = $query->orderBy('id', 'DESC');
        $order = $query->get();
        $order->load(['customer', 'armada', 'routes','orderdetailcosts.cost','budgetdetails.cost','user.role','itemdetails']);
        
        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);

    }

    public function orderStatusPending(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable|exists:armadas,id',
            'user_id' => 'nullable|exists:users,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // Pending :
        // 1. On Progress
        // 6. Pending
        // 3. Verified
        // 2. Delivered

        $query = Order::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('user_id')) {
            $query = $query->whereUser_id($request->user_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status_order',[7,6,3,2,1]);
        $query = $query->orderBy('id', 'DESC');
        $order = $query->get();
        $order->load(['customer', 'armada', 'routes','user.role','itemdetails']);
        
        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);
        
    }

    public function orderStatusHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'nullable|exists:armadas,id',
            'user_id' => 'nullable|exists:users,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // History :
        // 4. Closed
        // 5. Cancel
        //

        $query = Order::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('armada_id')) {
            $query = $query->whereArmada_id($request->armada_id);
        }

        if ($request->has('user_id')) {
            $query = $query->whereUser_id($request->user_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status_order',[4,5]);
        $query = $query->orderBy('id', 'DESC');
        $order = $query->get();
        $order->load(['customer', 'armada', 'routes','orderdetailcosts.cost','budgetdetails.cost','user.role','itemdetails']);
        
        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);

    }

    public function orderDriverStatusOpen(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'user_id' => 'required|exists:users,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // Open :
        // 0. Open

        $query = Order::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('user_id')) {
            $query = $query->whereUser_id($request->user_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status_order',[0]);
        $query = $query->orderBy('id', 'DESC');
        $order = $query->get();
        $order->load(['customer', 'armada', 'routes','user.role','itemdetails']);
        
        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);

    }

    public function orderDriverStatusProgress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'user_id' => 'required|exists:users,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // Progress :
        // 1. On progress

        $query = Order::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('user_id')) {
            $query = $query->whereUser_id($request->user_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status_order',[1]);
        $query = $query->orderBy('id', 'ASC');
        $order = $query->get();
        $order->load(['customer', 'armada', 'routes','user.role','itemdetails']);
        
        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);

    }

    public function orderDriverStatusVerified(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'user_id' => 'required|exists:users,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        // Progress :
        // 3. Verified

        $query = Order::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('user_id')) {
            $query = $query->whereUser_id($request->user_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status_order',[3]);
        $query = $query->orderBy('id', 'ASC');
        $order = $query->get();
        $order->load(['customer', 'armada', 'routes','user.role','itemdetails']);
        
        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);

    }

    public function orderDriverStatusLoaded(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'user_id' => 'required|exists:users,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Order::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('user_id')) {
            $query = $query->whereUser_id($request->user_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status_order', [7]);
        $query = $query->orderBy('id', 'ASC');
        $order = $query->get();
        $order->load(['customer', 'armada', 'routes','user.role','itemdetails']);
        
        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);

    }

    public function orderDriverStatusDelivered(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'user_id' => 'required|exists:users,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Order::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('user_id')) {
            $query = $query->whereUser_id($request->user_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereIn('status_order', [2]);
        $query = $query->orderBy('id', 'ASC');
        $order = $query->get();
        $order->load(['customer', 'armada', 'routes','user.role','itemdetails']);
        
        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
            'user_id' => 'required|exists:users,id',
            'customer_id' => 'required|exists:customers,id',
            'departure_date' => 'required|date_format:Y-m-d H:i:s',
            'arrival_date' => 'required|date_format:Y-m-d H:i:s',
            //'item_type' => 'present|nullable|string',
            'item_weight' => 'present|nullable|string',
            'item_qty' => 'present|nullable|string',
            'item_detail' => 'present|nullable|string',
            'image_item' => 'present|nullable|string',
            'image_item2' => 'present|nullable|string',
            'image_item3' => 'present|nullable|string',
            'bruto' => 'nullable|numeric',
            'disc' => 'nullable|numeric',
            'disc_value' => 'nullable|numeric',
            'ppn' => 'nullable|numeric',
            'ppn_value' => 'nullable|numeric',
            'pph' => 'nullable|numeric',
            'pph_value' => 'nullable|numeric',
            'netto' => 'nullable|numeric',
            'total_budgetvalue' => 'nullable|numeric',
            'driver_note' => 'present|nullable|string',
            'status_order' => 'nullable|integer',
            // validasi di hapus
            //'itemdetails.*.item_type' => 'present|string',
            // 'itemdetails.*.item_code' => 'present|string',
            // 'itemdetails.*.item_name' => 'present|string',
            // 'itemdetails.*.unit' => 'present|string',
            // 'itemdetails.*.qty' => 'present|string',
            // 'itemdetails.*.weight' => 'present|numeric',
            // 'itemdetails.*.dimensi' => 'present|numeric',
            // 'itemdetails.*.type' => 'present|int',
            'routes.*.name' => 'present|nullable|string',
            'routes.*.address' => 'required|string',
            'routes.*.latitude' => 'present|nullable',
            'routes.*.longitude' => 'present|nullable',
            'routes.*.pic_name' => 'required|string',
            'routes.*.pic_contact' => 'required|numeric',
            'routes.*.status' => 'required|integer',
            'budgetdetails.*.cost_id' => 'nullable|exists:costs,id',
            'budgetdetails.*.value' => 'nullable|numeric',
            'orderdetailcosts.*.cost_id' => 'nullable|exists:costs,id',
            'orderdetailcosts.*.value' => 'nullable|numeric',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $image_item_url = '';

        if ($request->has('image_item')) {
            if (preg_match('/^data:image\/(\w+);base64,/', $request->image_item)) {
                $image_item = substr($request->image_item, strpos($request->image_item, ",")+1);
                $image_item_name = time() . '.' . str_random(10) . '.jpg';
                $image_item_path = public_path() . "/uploads/order/" . $image_item_name;
                file_put_contents($image_item_path, base64_decode($image_item));
                $image_item_url = config('global.url'). "/uploads/order/" . $image_item_name;
            }
        } else {
            $image_item_url = config('global.url'). "/uploads/no_image.jpg";
        }

        $image_item_url2 = '';

        if ($request->has('image_item2')) {
            if (preg_match('/^data:image\/(\w+);base64,/', $request->image_item2)) {
                $image_item2 = substr($request->image_item2, strpos($request->image_item2, ",")+1);
                $image_item_name2 = time() . '.' . str_random(10) . '.jpg';
                $image_item_path2 = public_path() . "/uploads/order/" . $image_item_name2;
                file_put_contents($image_item_path2, base64_decode($image_item2));
                $image_item_url2 = config('global.url'). "/uploads/order/" . $image_item_name2;
            }
        } else {
            $image_item_url2 = config('global.url'). "/uploads/no_image.jpg";
        }

        $image_item_url3 = '';

        if ($request->has('image_item3')) {
            if (preg_match('/^data:image\/(\w+);base64,/', $request->image_item3)) {
                $image_item3 = substr($request->image_item3, strpos($request->image_item3, ",")+1);
                $image_item_name3 = time() . '.' . str_random(10) . '.jpg';
                $image_item_path3 = public_path() . "/uploads/order/" . $image_item_name3;
                file_put_contents($image_item_path3, base64_decode($image_item3));
                $image_item_url3 = config('global.url'). "/uploads/order/" . $image_item_name3;
            }
        } else {
            $image_item_url3 = config('global.url'). "/uploads/no_image.jpg";
        }

        //calculate
        $bruto = array_sum(array_column($request->orderdetailcosts, 'value'));
        $disc = $request->disc; // value cannot be comma
        $disc_value = $bruto * $disc / 100;
        $sub_total = $bruto - $disc_value;
        $ppn = $request->ppn; // value cannot be comma
        $ppn_value = $sub_total * $ppn / 100;
        $pph = $request->pph; // value cannot be comma
        $pph_value = $sub_total * $pph / 100;
        $netto = $sub_total + $ppn_value - $pph_value;
        $total_budgetvalue = array_sum(array_column($request->budgetdetails, 'value'));
        $no_order = $this->countercodeORD($request->company_id, $request->customer_id);

        $order = Order::create([
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'user_id' => $request->user_id,
            'customer_id' => $request->customer_id,
            'no_order' => $no_order,
            'order_date' => date("Y-m-d"),
            'departure_date' => $request->departure_date,
            'arrival_date' => $request->arrival_date,
            'item_type' => $request->item_type,
            'item_weight' => $request->item_weight,
            'item_qty' => $request->item_qty,
            'item_detail' => $request->item_detail,
            'image_item' => $image_item_url,
            'image_item2' => $image_item_url2,
            'image_item3' => $image_item_url3,
            'bruto' => $bruto,
            'disc' => $disc,
            'disc_value' => $disc_value,
            'ppn' => $ppn,
            'ppn_value' => $ppn_value,
            'pph' => $pph,
            'pph_value' => $pph_value,
            'netto' => $netto,
            'payment' => 0,
            'outstanding' => $netto,
            'total_budgetvalue' => $total_budgetvalue,
            'driver_note' => $request->driver_note,
            'status_order' => $request->status_order,
            'order_id_digdeplus' => $request->order_id_digdeplus
        ]);

        // Armada::where("id", $request->armada_id)->update(['status' => 1]);
        // User::where("id", $request->user_id)->update(['status' => 1]);

        // Progress :
        // 0. Open
        // 1. On progress
        // Pending :
        // 6. Pending
        // 3. Verified
        // 2. Delivered
        // History :
        // 4. Closed
        // 5. Cancel

        if ($request->status_order == 1) {
            $order->update([
                'driver_departure_date' => now(),
                'status_order' => 1
            ]);
        } elseif ($request->status_order == 2) {
            $order->update([
                'driver_arrival_date' => now(),
                'status_order' => 2
            ]);

            $start  = new Carbon($order->driver_departure_date);
            $end    = new Carbon($order->driver_arrival_date);

            $hours = $end->diffInHours($start);
            $minutes = $end->diffInMinutes($start) % 60;

            $order->update([
                'driver_actual_hours' => (string)$hours,
                'driver_actual_minutes' => (string)$minutes
            ]);
        } elseif ($request->status_order == 4) {
            $order->update(['status_order' => 4]);

            $userAdmin = User::whereCompany_id($order->company_id)
                            ->whereRole_id(2)->get();
            $userDriver = User::whereId($order->user_id)->get();
            $merged = $userAdmin->merge($userDriver);
            $user = $merged->all();

            $details = [
                'no_order' => $order->no_order
            ];

            Notification::send($user, new NotificationCloseOrder($details));
        } else {
            $order->update(['status_order' => $request->status_order]);
        } 

        $routes = [];
        foreach ($request->routes as $row) {
            array_push($routes, [
                'company_id' => $request->company_id,
                'name'=> $row['name'], 
                'address' => $row['address'],
                'latitude' => $row['latitude'],
                'longitude' => $row['longitude'],
                'pic_name' => $row['pic_name'],
                'pic_contact' => $row['pic_contact'],
                'status'=> $row['status'],
            ]);
        }

        $budgetdetails = [];
        foreach ($request->budgetdetails as $row) {
            array_push($budgetdetails, [
                'company_id' => $request->company_id,
                'cost_id' => $row['cost_id'],
                'value' => $row['value'],
            ]);
        }

        $itemdetails = [];
        foreach($request->itemdetails as $row){
            array_push($itemdetails, [
                'order_id' => $request->order_id,
                'item_type' => $row['item_type_baru'],
                'item_code' => $row['item_code'],
                'item_name' => $row['item_name'],
                'unit' => $row['unit'],
                'qty' => $row['qty'],
                'weight' => $row['weight'],
                'dimensi' => $row['dimensi'],
                'type' => $row['type'],
            ]);
        }

        //insert multiple related models
        $order->routes()->createMany($routes);
        $order->budgetdetails()->createMany($budgetdetails);
        $order->itemdetails()->createMany($itemdetails);


//        $costs = Cost::select('id')->whereCompany_id($request->company_id)->whereStatus(0)->get();
//        $orderdetailcosts = [];
//        foreach ($costs as $cost) {
//            array_push($orderdetailcosts, [
//                'company_id' => $request->company_id,
//                'cost_id' => $cost->id,
//                'value' => 0,
//            ]);
//        }
        $orderdetailcosts = [];
        if ($request->has('orderdetailcosts')){
            foreach ($request->orderdetailcosts as $cost) {
                array_push($orderdetailcosts, [
                    'company_id' => $request->company_id,
                    'cost_id' => $cost['cost_id'],
                    'value' => $cost['value'],
                ]);
            }
        }

        $order->orderdetailcosts()->createMany($orderdetailcosts);

        $requestorderdetailcosts = [];
        foreach ($request->orderdetailcosts as $orderdetailcost) {
            array_push($requestorderdetailcosts, [
                'company_id' => $request->company_id,
                'cost_id' => $orderdetailcost['cost_id'],
                'value' => $orderdetailcost['value'],
            ]);
        }


        if (count($requestorderdetailcosts)) {
            foreach ($requestorderdetailcosts as $requestorderdetailcost) {
                $orderdetailcost = Orderdetailcost::whereOrder_id($order->id)
                                ->whereCost_id($requestorderdetailcost['cost_id'])
                                ->first();

                if ($orderdetailcost) {
                    $orderdetailcost->update([
                        'cost_id' => $requestorderdetailcost['cost_id'], 
                        'value' => $requestorderdetailcost['value'],
                    ]);
                }
            }
        }

        //insert Schedule booking
        $this->insertScheduleArmada($order);
        $this->insertScheduleUser($order);

        $data = Order::with(['customer', 'routes', 'budgetdetails.cost', 'orderdetailcosts.cost','itemdetails'])
                ->whereId($order->id)
                ->get();

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $data[0]
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::with(['customer', 'armada', 'user.role', 'routes', 'budgetdetails.cost', 'orderdetailcosts.cost', 'itemdetails'])
                ->whereCompany_id($request->company_id)->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
            'user_id' => 'required|exists:users,id',
            'customer_id' => 'required|exists:customers,id',
            'departure_date' => 'required|date_format:Y-m-d H:i:s',
            'arrival_date' => 'required|date_format:Y-m-d H:i:s',
            // 'item_type' => 'present|nullable|string',
            'item_weight' => 'present|nullable|string',
            'item_qty' => 'present|nullable|string',
            'item_detail' => 'present|nullable|string',
            'image_item' => 'present|nullable|string',
            'image_item2' => 'present|nullable|string',
            'image_item3' => 'present|nullable|string',
            'bruto' => 'required|numeric',
            'disc' => 'required|numeric',
            'disc_value' => 'required|numeric',
            'ppn' => 'required|numeric',
            'ppn_value' => 'required|numeric',
            'pph' => 'required|numeric',
            'pph_value' => 'required|numeric',
            'netto' => 'required|numeric',
            'total_budgetvalue' => 'required|numeric',
            'driver_note' => 'present|nullable|string',
            'status_order' => 'required|integer',
            // 'itemdetails.*.item_type' => 'present|string',
            // 'itemdetails.*.item_code' => 'present|string',
            // 'itemdetails.*.item_name' => 'present|string',
            // 'itemdetails.*.unit' => 'present|string',
            // 'itemdetails.*.qty' => 'present|string',
            // 'itemdetails.*.weight' => 'present|numeric',
            // 'itemdetails.*.dimensi' => 'present|numeric',
            // 'itemdetails.*.type' => 'present|int',
            'routes.*.name' => 'present|nullable|string',
            'routes.*.address' => 'required|string',
            'routes.*.latitude' => 'present|nullable',
            'routes.*.longitude' => 'present|nullable',
            'routes.*.pic_name' => 'required|string',
            'routes.*.pic_contact' => 'required|numeric',
            'routes.*.status' => 'required|integer',
            'budgetdetails.*.cost_id' => 'required|exists:costs,id',
            'budgetdetails.*.value' => 'required|numeric',
            'orderdetailcosts.*.cost_id' => 'required|exists:costs,id',
            'orderdetailcosts.*.value' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::whereCompany_id($request->company_id)->findOrFail($id);

        $image_item_url = '';
        if ($request->has('image_item')) {
            if (preg_match('/^data:image\/(\w+);base64,/', $request->image_item)) {
                $image_item = substr($request->image_item, strpos($request->image_item, ",")+1);
                $image_item_name = time() . '.' . str_random(10) . '.jpg';
                $image_item_path = public_path() . "/uploads/order/" . $image_item_name;
                file_put_contents($image_item_path, base64_decode($image_item));
                $image_item_url = config('global.url'). "/uploads/order/" . $image_item_name;
                $order->update([
                    'image_item' => $image_item_url,
                ]);
            }
        }

        $image_item_url2 = '';
        if ($request->has('image_item2')) {
            if (preg_match('/^data:image\/(\w+);base64,/', $request->image_item2)) {
                $image_item2 = substr($request->image_item2, strpos($request->image_item2, ",")+1);
                $image_item_name2 = time() . '.' . str_random(10) . '.jpg';
                $image_item_path2 = public_path() . "/uploads/order/" . $image_item_name2;
                file_put_contents($image_item_path2, base64_decode($image_item2));
                $image_item_url2 = config('global.url'). "/uploads/order/" . $image_item_name2;
                $order->update([
                    'image_item2' => $image_item_url2,
                ]);
            }
        }

        $image_item_url3 = '';

        if ($request->has('image_item3')) {
            if (preg_match('/^data:image\/(\w+);base64,/', $request->image_item3)) {
                $image_item3 = substr($request->image_item3, strpos($request->image_item3, ",")+1);
                $image_item_name3 = time() . '.' . str_random(10) . '.jpg';
                $image_item_path3 = public_path() . "/uploads/order/" . $image_item_name3;
                file_put_contents($image_item_path2, base64_decode($image_item3));
                $image_item_url3 = config('global.url'). "/uploads/order/" . $image_item_name3;
                $order->update([
                    'image_item3' => $image_item_url3,
                ]);
            }
        }

        //calculate
        $bruto = array_sum(array_column($request->orderdetailcosts, 'value'));
        $disc = $request->disc; // value cannot be comma
        $disc_value = $bruto * $disc / 100;
        $sub_total = $bruto - $disc_value;
        $ppn = $request->ppn; // value cannot be comma
        $ppn_value = $sub_total * $ppn / 100;
        $pph = $request->pph; // value cannot be comma
        $pph_value = $sub_total * $pph / 100;
        $netto = $sub_total + $ppn_value - $pph_value;
        $total_budgetvalue = array_sum(array_column($request->budgetdetails, 'value'));

        $order->update([
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'user_id' => $request->user_id,
            'customer_id' => $request->customer_id,
            // 'no_order' => $no_order,
            // 'order_date' => date("Y-m-d"),
            'departure_date' => $request->departure_date,
            'arrival_date' => $request->arrival_date,
            'item_type' => $request->item_type,
            'item_weight' => $request->item_weight,
            'item_qty' => $request->item_qty,
            'item_detail' => $request->item_detail,
            'image_item' => $image_item_url,
            'image_item2' => $image_item_url2,
            'image_item3' => $image_item_url3,
            'bruto' => $bruto,
            'disc' => $disc,
            'disc_value' => $disc_value,
            'ppn' => $ppn,
            'ppn_value' => $ppn_value,
            'pph' => $pph,
            'pph_value' => $pph_value,
            'netto' => $netto,
            'payment' => 0,
            'outstanding' => $netto,
            'total_budgetvalue' => $total_budgetvalue,
            'driver_note' => $request->driver_note,
            'status_order' => $request->status_order,
            ]);

        // Progress :
        // 0. Open
        // 1. On progress
        // Pending :
        // 6. Pending
        // 3. Verified
        // 2. Delivered
        // History :
        // 4. Closed
        // 5. Cancel

        if ($request->status_order == 1) {
            $order->update([
                'driver_departure_date' => now(),
                'status_order' => 1
            ]);
        } elseif ($request->status_order == 2) {

            $order->update([
                'driver_arrival_date' => now(),
                'status_order' => 2
            ]);

            $driver_departure_date = new DateTime($order->driver_departure_date);
            $driver_arrival_date = new DateTime($order->driver_arrival_date);
            $interval = $driver_departure_date->diff($driver_arrival_date);
            $driver_actual_hours   = $interval->format('%h');
            $driver_actual_minutes = $interval->format('%i');

            $order->update([
                'driver_actual_hours' => $driver_actual_hours,
                'driver_actual_minutes' => $driver_actual_minutes
            ]);



        } elseif ($request->status_order == 4) {

            $order->update(['status_order' => 4]);

            $userAdmin = User::whereCompany_id($order->company_id)
                            ->whereRole_id(2)->get();
            $userDriver = User::whereId($order->user_id)->get();
            $merged = $userAdmin->merge($userDriver);
            $user = $merged->all();

            $details = [
                'no_order' => $order->no_order
            ];

            Notification::send($user, new NotificationCloseOrder($details));
        } else {
            $order->update(['status_order' => $request->status_order]);
        }

        $routes = [];
        foreach ($request->routes as $row) {
            array_push($routes, [
                'company_id' => $request->company_id,
                'name'=> $row['name'],
                'address' => $row['address'],
                'latitude' => $row['latitude'],
                'longitude' => $row['longitude'],
                'pic_name' => $row['pic_name'],
                'pic_contact' => $row['pic_contact'],
                'status'=> $row['status'],
            ]);
        }

        $budgetdetails = [];
        if ($request->has('budgetdetails')){
            foreach ($request->budgetdetails as $row) {
                array_push($budgetdetails, [
                    'company_id' => $request->company_id,
                    'cost_id' => $row['cost_id'],
                    'value' => $row['value'],
                    'realization' => $row['realization']
                ]);
            }
        }

        $itemdetails = [];
        if ($request->has('itemdetails')){
            foreach ($request->itemdetails as $row) {
                array_push($itemdetails, [
                    'order_id' => $request->order_id,
                    'item_type' => $row['item_type_baru'],
                    'item_code' => $row['item_code'],
                    'item_name' => $row['item_name'],
                    'unit' => $row['unit'],
                    'qty' => $row['qty'],
                    'weight' => $row['weight'],
                    'dimensi' => $row['dimensi'],
                    'type' => $row['type'],

                ]);
            }
        }

        //Delete then insert multiple related models
        Route::whereOrder_id($id)->forceDelete();
        Budgetdetail::whereOrder_id($id)->forceDelete();
        Orderdetailcost::whereOrder_id($id)->forceDelete();
        Itemdetail::whereOrder_id($id)->forceDelete();

        $order->routes()->createMany($routes);
        $order->budgetdetails()->createMany($budgetdetails);
        $order->itemdetails()->createMany($itemdetails);

//        $costs = Cost::select('id')->whereCompany_id($request->company_id)->whereStatus(0)->get();
//        $orderdetailcosts = [];
//        foreach ($costs as $cost) {
//            array_push($orderdetailcosts, [
//                'company_id' => $request->company_id,
//                'cost_id' => $cost->id,
//                'value' => 0,
//            ]);
//        }

        $orderdetailcosts = [];

        if ($request->has('orderdetailcosts')){
            foreach ($request->orderdetailcosts as $cost) {
                array_push($orderdetailcosts, [
                    'company_id' => $request->company_id,
                    'cost_id' => $cost['cost_id'],
                    'value' => $cost['value'],
                ]);
            }
        }

        $order->orderdetailcosts()->createMany($orderdetailcosts);

        $requestorderdetailcosts = [];
        if ($request->has('orderdetailcosts')) {
            foreach ($request->orderdetailcosts as $orderdetailcost) {
                array_push($requestorderdetailcosts, [
                    'company_id' => $request->company_id,
                    'cost_id' => $orderdetailcost['cost_id'],
                    'value' => $orderdetailcost['value'],
                ]);
            }
        }

        if (count($requestorderdetailcosts)) {
            foreach ($requestorderdetailcosts as $requestorderdetailcost) {
                $orderdetailcost = Orderdetailcost::whereOrder_id($order->id)
                                ->whereCost_id($requestorderdetailcost['cost_id'])
                                ->first();

                if ($orderdetailcost) {
                    $orderdetailcost->update([
                        'cost_id' => $requestorderdetailcost['cost_id'],
                        'value' => $requestorderdetailcost['value'],
                    ]);
                }
            }
        }

        //Delete then insert Schedule booking
        Schedulearmada::whereNumber($order->no_order)->delete();
        Scheduleuser::whereNumber($order->no_order)->delete();

        // Check If status not delievered or closed
        // After delete then Insert again
        if ($request->status_order != 2 && $request->status_order != 4 ){
            $this->insertScheduleArmada($order);
            $this->insertScheduleUser($order);
        }

        $data = Order::with(['customer', 'armada', 'routes', 'budgetdetails.cost', 'orderdetailcosts.cost', 'itemdetails'])
                ->whereId($order->id)
                ->first();

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => $data
        ];

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::whereCompany_id($request->company_id)->findOrFail($id);

        //Delete Schedule booking
        Schedulearmada::whereNumber($order->no_order)->delete();
        Scheduleuser::whereNumber($order->no_order)->delete();

        $order->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }

    public function updateStatus(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'status_order' => 'required|integer',
            'file_signed' => 'present|nullable|string',
            'image_item' => 'present|nullable|string',
            'image_item2' => 'present|nullable|string',
            'image_item3' => 'present|nullable|string',

        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

            if (preg_match('/^data:image\/(\w+);base64,/', $request->file_signed)) {
                $file_signed = substr($request->file_signed, strpos($request->file_signed, ",")+1);
                $file_signed_name = time() . '.' . str_random(10) . '.jpg';
                $file_signed_path = public_path() . "/uploads/order/" . $file_signed_name;
                file_put_contents($file_signed_path, base64_decode($file_signed));
                $file_signed = config('global.url'). "/uploads/order/" . $file_signed_name;
            }

            if (preg_match('/^data:image\/(\w+);base64,/', $request->image_item)) {
                $image_item = substr($request->image_item, strpos($request->image_item, ",")+1);
                $image_item_name = time() . '.' . str_random(10) . '.jpg';
                $image_item_path = public_path() . "/uploads/order/" . $image_item_name;
                file_put_contents($image_item_path, base64_decode($image_item));
                $image_item = config('global.url'). "/uploads/order/" . $image_item_name;
            }

            if (preg_match('/^data:image\/(\w+);base64,/', $request->image_item2)) {
                $image_item2 = substr($request->image_item2, strpos($request->image_item2, ",")+1);
                $image_item_name2 = time() . '.' . str_random(10) . '.jpg';
                $image_item_path2 = public_path() . "/uploads/order/" . $image_item_name2;
                file_put_contents($image_item_path2, base64_decode($image_item2));
                $image_item2 = config('global.url'). "/uploads/order/" . $image_item_name2;
            }

            if (preg_match('/^data:image\/(\w+);base64,/', $request->image_item3)) {
                $image_item3 = substr($request->image_item3, strpos($request->image_item3, ",")+1);
                $image_item_name3 = time() . '.' . str_random(10) . '.jpg';
                $image_item_path3 = public_path() . "/uploads/order/" . $image_item_name3;
                file_put_contents($image_item_path3, base64_decode($image_item3));
                $image_item3 = config('global.url'). "/uploads/order/" . $image_item_name3;
            }

        // Progress :
        // 0. Open
        // 1. On progress
        // Pending :
        // 6. Pending
        // 3. Verified
        // 2. Delivered
        // 7. Loaded
        // History :
        // 4. Closed
        // 5. Cancel
            // dd($file_signed);
        $order = Order::with(['customer', 'armada', 'routes', 'budgetdetails.cost', 'orderdetailcosts.cost','itemdetails'])
                ->whereCompany_id($request->company_id)->findOrFail($id);
        
        if ($request->status_order == 1) {
            // if($order->status_order >= $request->status_order){
                
            //     $response = [
            //         'status' => 'error',
            //         'message' => 'Order sudah di kirim !',
            //         'data' => null
            //     ];
            //         return response()->json($response, 400);
            //     }

            $order->update([
                'driver_departure_date' => now(),
                'status_order' => 1
            ]);

            Schedulearmada::where(['number' => $order->no_order])->update(['status_order' => 1 ]);
            Scheduleuser::where(['number' => $order->no_order])->update(['status_order' => 1 ]);

        } elseif ($request->status_order == 2) {
            
            //Delete Schedule booking
            Schedulearmada::whereNumber($order->no_order)->delete();
            Scheduleuser::whereNumber($order->no_order)->delete();

            // dd($file_signed);
            $order->update([
                'driver_arrival_date' => now(),
                'status_order' => 2,
                'file_signed' => $file_signed
            ]);

            $driver_departure_date = new DateTime($order->driver_departure_date);
            $driver_arrival_date = new DateTime($order->driver_arrival_date);
            $interval = $driver_departure_date->diff($driver_arrival_date);
            $driver_actual_hours   = $interval->format('%h');
            $driver_actual_minutes = $interval->format('%i');

            $order->update([
                'driver_actual_hours' => $driver_actual_hours,
                'driver_actual_minutes' => $driver_actual_minutes
            ]);

            Schedulearmada::where(['number' => $order->no_order])->update(['status_order' => 2 ]);
            Scheduleuser::where(['number' => $order->no_order])->update(['status_order' => 2 ]);

             if($order->customer->is_digdeplus == true){
                $this->UpdateStatusDigdeplus($order->order_id_digdeplus,$file_signed);
            }

        } elseif ($request->status_order == 4) {
            //Delete Schedule booking
            Schedulearmada::whereNumber($order->no_order)->delete();
            Scheduleuser::whereNumber($order->no_order)->delete();

            $order->update(['status_order' => 4]);

            $userAdmin = User::whereCompany_id($order->company_id)
                            ->whereRole_id(2)->get();
            $userDriver = User::whereId($order->user_id)->get();
            $merged = $userAdmin->merge($userDriver);
            $user = $merged->all();

            $details = [
                'no_order' => $order->no_order
            ];

            Notification::send($user, new NotificationCloseOrder($details));

            // amin tambah kondisi armada
            // Armada::where(['id' => $order->armada_id])->update(['status' => 0]);

            // amin tambah kondisi driver
            User::where(['id' => $order->user_id])->update(['status' => 2]);

        } elseif ($request->status_order == 5){

            $order->update(['status_order' => 5]);

            //delete schedule booking
            Schedulearmada::whereNumber($order->no_order)->delete();
            Scheduleuser::whereNumber($order->no_order)->delete();

            // amin tambah kondisi armada
            // Armada::where(['id' => $order->armada_id])->update(['status' => 0]);

            // amin tambah kondisi driver
            User::where(['id' => $order->user_id])->update(['status' => 2]);

        } elseif($request->status_order == 7){

            $order->update([
                'driver_departure_date' => now(),
                'status_order' => 7,
                'file_signed' => $request->file_signed,
                'image_item' => $request->image_item,
                'image_item2' => $request->image_item2,
                'image_item3' => $request->image_item3
            ]);


            Schedulearmada::where(['number' => $order->no_order])->update(['status_order' => 7 ]);
            Scheduleuser::where(['number' => $order->no_order])->update(['status_order' => 7 ]);
        } else {
            $order->update(['status_order' => $request->status_order]);


            Schedulearmada::where(['number' => $order->no_order])->update(['status_order' => $request->status_order ]);
            Scheduleuser::where(['number' => $order->no_order])->update(['status_order' => $request->status_order ]);
        } 

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => $order
        ];
        return response()->json($response, 200);
    }

    public function updateStatusDetail(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:item_details,id',
            'item_code' => 'present|string',
            'order_id' => 'present|integer',
            'type' => 'required|integer'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Itemdetail::query()
                ->whereId($request->id)->findOrFail($id);
        
        $order->update([
            'item_code' => $request->item_code,
            'order_id' => $request->order_id,
            'type' => $request->type
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => $order
        ];
        return response()->json($response, 200);

    }

    public function showRating($id)
    {
        $order = Order::findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);
    }

    public function updateRating(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'disciplinerating' => 'required|integer',
            'servicerating' => 'required|integer',
            'safetyrating' => 'required|integer',
            'commentrating' => 'present|nullable',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::findOrFail($id);
        $order->update($request->all());

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => Order::findOrFail($id)
        ];
        return response()->json($response, 200);
    }

    public function sj(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::with(['company','customer', 'routes', 'budgetdetails.cost', 'orderdetailcosts.cost','itemdetails'])
                ->whereCompany_id($request->company_id)->findOrFail($id);
        
        $orderdetail = ItemDetail::query()->whereOrderId($id)->get();

        $company = Company::query()->whereId($request->company_id)->get();

        // return dd($orderdetail);
        //Update SJ
        if (empty($order->sj_date)) {
            $parts = explode("-", $order->no_order);
            $no_sj = 'SJ/'.$parts[1];

            $order->update([
                'no_sj' => $no_sj,
                'sj_date' => Carbon::now()->toDateString()
            ]);
        }

        $suratjalan = 'surat_jalan.pdf';
        $pdf = PDF::loadview('pdf.sj',['order' => $order,'company' => $company,'orderdetail' => $orderdetail])
            ->setPaper('A4', 'portrait');

        return $pdf->download($suratjalan);
    }

    public function tandaterima(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::with(['customer', 'routes', 'budgetdetails.cost', 'orderdetailcosts.cost','itemdetails'])
                ->whereCompany_id($request->company_id)->findOrFail($id);

        $orderdetail = ItemDetail::query()->whereOrderId($id)->get();

        $company = Company::query()->whereId($request->company_id)->get();


        //Update SJ
        if (empty($order->sj_date)) {
            $parts = explode("-", $order->no_order);
            $no_sj = 'SJ/'.$parts[1];

            $order->update([
                'no_sj' => $no_sj,
                'sj_date' => Carbon::now()->toDateString()
            ]);
        }

        $tandaterima = 'tanda_terima.pdf';
        $pdf = PDF::loadview('pdf.tt',['order' => $order,'company' => $company, 'orderdetail' => $orderdetail])
            ->setPaper('A4', 'portrait');
        return $pdf->download($tandaterima);
    }

    public function inv(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $order = Order::with(['customer', 'routes', 'budgetdetails.cost', 'orderdetailcosts.cost','itemdetails'])
                ->whereCompany_id($request->company_id)->findOrFail($id);

        $company = Company::query()->whereId($request->company_id)->get();

        //Update Invoice
        if (empty($order->invoice_date)) {
            $parts = explode("-", $order->no_order);
            $no_invoice = 'INV/'.$parts[1];
            $term = $order->customer->term;
            $due_date = Carbon::now()->addDays($term)->toDateString();

            $order->update([
                'no_invoice' => $no_invoice,
                'invoice_date' => Carbon::now()->toDateString(),
                'due_date' => $due_date
            ]);
        }

        $invoice = 'Invoice.pdf';
        $pdf = PDF::loadView('pdf.invoice',['order' => $order, 'company' => $company])
            ->setPaper('A4', 'portrait');

        return $pdf->download($invoice);

    }

    public function invoices(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Order::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $query = $query->where('outstanding', '!=', 0);
        $query = $query->orderBy('due_date','ASC');
        $invoices = $query->get();
        $invoices->load('customer');
        
        $response = [
            'status' => 'success',
            'data' => $invoices
        ];
        return response()->json($response, 200);
    }

    public function countercodeORD($company_id, $customer_id)
    {
        $company_id = str_pad($company_id,3,0,STR_PAD_LEFT);
        $customer_id = str_pad($customer_id,3,0,STR_PAD_LEFT);
        $countercode = Countercode::whereType('ORD')
                    ->whereFor($company_id)
                    ->whereYear('max_date','=', date('Y'))->first();

        if(!$countercode){
            $insert = Countercode::insert([
                    'type' => 'ORD',
                    'for'=>$company_id,
                    'lastcounter'=> 1,
                    'max_date' => date('Y').'-12-31',
                ]);

            $nxt = 1;
        }
        else{ 
            $nxt = $countercode->lastcounter + 1; 
            $update = Countercode::where('id',$countercode->id)->update(['lastcounter'=>$nxt]);
        }

        $nxt = str_pad($nxt,4,0,STR_PAD_LEFT);
        $code = 'ORD'.'/'.$company_id.'-'.$customer_id.'/'.date('m').date('y').'/'.$nxt;
        
        return $code;
    }

    public function insertScheduleArmada($order)
    {
        $period = CarbonPeriod::create($order->departure_date, $order->arrival_date);

        $data = [];
        // Iterate over the period
        foreach ($period as $date) {
            array_push($data, [
                'company_id' => $order->company_id,
                'armada_id' => $order->armada_id,
                'date' => $date->format('Y-m-d'),
                'number' => $order->no_order,
                'type' => 'ORD',
                'status_order' => $order->status_order
            ]);
        }
        Schedulearmada::insert($data);
    }

    public function insertScheduleUser($order)
    {
        $period = CarbonPeriod::create($order->departure_date, $order->arrival_date);

        $data = [];
        // Iterate over the period
        foreach ($period as $date) {
            array_push($data, [
                'company_id' => $order->company_id,
                'user_id' => $order->user_id,
                'date' => $date->format('Y-m-d'),
                'number' => $order->no_order,
                'type' => 'ORD',
                'status_order' => $order->status_order
            ]);
        }
        Scheduleuser::insert($data);
    }

    public function UpdateStatusDigdeplus($id,$image){
        
        $client = new \GuzzleHttp\Client();

        $headers = [
            'Content-Type' => 'application/json'
        ];

        $res = $client->request('POST',env('API_URL_DIGDEPLUS').'/api/digkontrol/update_status_delivered',[
            'auth' => ['digkontrol_access','$2y$10$Vy73ocAKJwuzxVR6LYQL8O7Z.yKarVs57Y67IFAB80aMFUFd1q.ou'
                      ],
            'headers' => $headers,
            'json'    => [  
                            "order_id"=>$id,
                            "file_signed"=>$image
            ]
        ]);
         $response = json_decode($res->getBody());
    }

    
}
