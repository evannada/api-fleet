<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Models\Schedulearmada;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Armada;
use App\Models\Scheduleuser;
use App\User;
use App\Models\Order;
use Carbon\Carbon;

class ScheduleController extends Controller
{
    public function scheduleArmadaAvailable(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'start_date' => 'required|date_format:Y-m-d H:i:s',
            'end_date' => 'required|date_format:Y-m-d H:i:s',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        //Todo Bikin FIltering juga
        $start=date('Y-m-d', strtotime($request->start_date));
        $end=date('Y-m-d', strtotime($request->end_date));

        $schedulearmada = Schedulearmada::whereCompany_id($request->company_id)
                        ->whereBetween('date', [$start, $end])
                        ->distinct()->get(['armada_id']);

        // dd($schedulearmada);

        $armada_id = [];
        foreach ($schedulearmada as $row) {
            array_push($armada_id,$row->armada_id);
        }

        // $query = Armada::query()->whereCompany_id($request->company_id);

        // $data = $query->with('schedulearmadas')->get();

        //Todo Bikin FIltering juga

        $armada = Armada::whereCompany_id($request->company_id)
                    ->with(['schedulearmadas' => function ($query) use ($request)
                    {
                        $query->whereBetween('date', [date('Y-m-d', strtotime($request->start_date)),date('Y-m-d', strtotime($request->start_date))]);
                    }])->get();

        // $armada = Armada::whereCompany_id($request->company_id)
        //         // ->whereNotIn('id', $armada_id)->get();
        //             ->with('schedulearmadas')->get();

        $response = [
            'status' => 'success',
            'data' => $armada
        ];
        return response()->json($response, 200);
    }

    public function scheduleDriverAvailable(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'start_date' => 'required|date_format:Y-m-d H:i:s',
            'end_date' => 'required|date_format:Y-m-d H:i:s',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        //TODO bikin filtering
        $start=date('Y-m-d', strtotime($request->start_date));
        $end=date('Y-m-d', strtotime($request->end_date));

        $Scheduleuser = Scheduleuser::whereCompany_id($request->company_id)
                        ->whereBetween('date', [$start, $end])
                        ->distinct()->get(['user_id']);

//        $Scheduleuser = Order::whereCompany_id($request->company_id)
////                        ->whereBetween('date', [$request->start_date, $request->end_date])
//                        ->where('departure_date','>=',$request->start_date)->where('arrival_date','<=',$request->end_date)
//                        ->whereIn('status_order', [0,1, 2, 3,6])
//                        ->distinct()->get(['user_id']);

        $user_id = [];
        foreach ($Scheduleuser as $row) {
            array_push($user_id, $row->user_id);
        }

        // $user = User::with('role')
        //         ->whereCompany_id($request->company_id)
        //         ->whereRole_id(3)
        //         ->whereNotIn('id', $user_id)->get();
        $user = User::with('role')
                ->whereCompany_id($request->company_id)
                ->whereRole_id(3)
                // ->whereNotIn('id', $user_id)
                ->with(['scheduleusers' => function ($query) use ($request)
                {
                    $query->whereBetween('date', [date('Y-m-d', strtotime($request->start_date)),date('Y-m-d', strtotime($request->start_date))]);
                }])->get();

        // return dd($user);

        $response = [
            'status' => 'success',
            'data' => $user
        ];
        return response()->json($response, 200);
    }

    public function scheduleArmadaOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $schedulearmada = Schedulearmada::whereCompany_id($request->company_id)
                        ->whereArmada_id($request->armada_id)
                        ->whereType('ORD')
                        ->where('date', Carbon::today()->toDateString())
                        ->first();

        if (!$schedulearmada) {
            $response = [
                'status' => 'success',
                'data' => $schedulearmada
            ];
            return response()->json($response, 200);
        }

        $order = Order::with(['customer', 'armada', 'routes', 'budgetdetails.cost', 'orderdetailcosts.cost'])
                    ->whereCompany_id($request->company_id)        
                    ->whereNo_order($schedulearmada->number)
                    ->first();

        $response = [
            'status' => 'success',
            'data' => $order
        ];
        return response()->json($response, 200);
    }
}
