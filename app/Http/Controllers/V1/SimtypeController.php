<?php

namespace App\Http\Controllers\V1;

use App\Models\Simtype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SimtypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $simtype = Simtype::all();
        $response = [
            'status' => 'success',
            'data' => $simtype
        ];
        return response()->json($response, 200);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $simtype = Simtype::create($request->all());
        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $simtype
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $simtype = Simtype::findOrFail($id);
        $response = [
            'status' => 'success',
            'data' => $simtype
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $simtype = Simtype::findOrFail($id);
        $simtype->update($request->all());
        
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => Simtype::find($id)
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $simtype = Simtype::findOrFail($id);
        $simtype->delete();
        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];

        return response()->json($response, 200);
    }
}
