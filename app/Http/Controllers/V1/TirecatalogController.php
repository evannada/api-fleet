<?php

namespace App\Http\Controllers\V1;

use App\Models\Tirecatalog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TirecatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tirecatalog = Tirecatalog::all();
        $response = [
            'status' => 'success',
            'data' => $tirecatalog
        ];
        return response()->json($response, 200);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'number_tires' => 'required|integer',
            'image_tirecatalog' => 'present|nullable|string'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_tirecatalog)) {
            $image_tirecatalog = substr($request->image_tirecatalog, strpos($request->image_tirecatalog, ",")+1);
            $image_tirecatalog_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_tirecatalog_path = public_path() . "/uploads/armada/tirecatalog/" . $image_tirecatalog_name;
            file_put_contents($image_tirecatalog_path, base64_decode($image_tirecatalog));
            $image_tirecatalog_url = config('global.url'). "/uploads/armada/tirecatalog/" . $image_tirecatalog_name;
        } else {
            $image_tirecatalog_url = config('global.url'). "/uploads/no_image.jpg";
        }

        $tirecatalog = Tirecatalog::create([
            'number_tires' => $request->number_tires,
            'image_tirecatalog' => $image_tirecatalog_url
        ]);
        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $tirecatalog
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tirecatalog = Tirecatalog::findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $tirecatalog
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'number_tires' => 'required|integer',
            'image_tirecatalog' => 'present|nullable|string'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $tirecatalog = Tirecatalog::findOrFail($id);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_tirecatalog)) {
            $image_tirecatalog = substr($request->image_tirecatalog, strpos($request->image_tirecatalog, ",")+1);
            $image_tirecatalog_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_tirecatalog_path = public_path() . "/uploads/armada/tirecatalog/" . $image_tirecatalog_name;
            file_put_contents($image_tirecatalog_path, base64_decode($image_tirecatalog));
            $image_tirecatalog_url = config('global.url'). "/uploads/armada/tirecatalog/" . $image_tirecatalog_name;
            $tirecatalog->update([
                'image_tirecatalog' => $image_tirecatalog_url
            ]);
        }

        $tirecatalog->update([
            'number_tires' => $request->number_tires,
            // 'image_tirecatalog' => $image_tirecatalog_url
        ]);
        
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' =>  $tirecatalog
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tirecatalog = Tirecatalog::findOrFail($id);
        $tirecatalog->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];

        return response()->json($response, 200);
    }
}
