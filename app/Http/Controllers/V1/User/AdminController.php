<?php

namespace App\Http\Controllers\V1\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Company;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = User::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $query = $query->whereRole_id(2);
        $user = $query->get();
        $user->load('role');

        $response = [
            'status' => 'success',
            'data' => $user
        ];
        return response()->json($response, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|string',
            'phone' => 'present|nullable|string',
            'image_profile' => 'present|nullable|string',
            'status' => 'required|integer'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $company = Company::whereId($request->company_id)->first();
        $number_user = $company->users()->whereIn('role_id', [2,3,4])->count();
        $max_user = $company->max_user;

        if ($number_user > $max_user) {
            return response()->json([
                'status' => 'error',
                'message' => 'You have exceeded the maximum number of users'
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_profile)) {
            $image_profile = substr($request->image_profile, strpos($request->image_profile, ",")+1);
            $image_profile_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_profile_path = public_path() . "/uploads/user/admin/profile/" . $image_profile_name;
            file_put_contents($image_profile_path, base64_decode($image_profile));
            $image_profile_url = config('global.url'). "/uploads/user/admin/profile/" . $image_profile_name;
        } else {
            $image_profile_url = config('global.url'). "/uploads/no_profile.jpg";
        }

        $user = User::create([
            'company_id' => $request->company_id,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'role_id' => 2,
            'remember_token' => str_random(40),
            'image_profile' => $image_profile_url,
            'status' => $request->status
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $user
        ], 200);
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('role')
                ->whereRole_id(2)        
                ->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $user
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'name' => 'required|string',
            // 'email' => 'required|email|unique:users,email',
            // 'password' => 'required|min:8|string',
            'phone' => 'present|nullable|string',
            'image_profile' => 'present|nullable|string',
            'status' => 'required|integer'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = User::whereRole_id(2)->findOrFail($id);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_profile)) {
            $image_profile = substr($request->image_profile, strpos($request->image_profile, ",")+1);
            $image_profile_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_profile_path = public_path() . "/uploads/user/admin/profile/" . $image_profile_name;
            file_put_contents($image_profile_path, base64_decode($image_profile));
            $image_profile_url = config('global.url'). "/uploads/user/admin/profile/" . $image_profile_name;
            $user->update([
                'image_profile' => $image_profile_url,
            ]);
        }

        $user->update([
            'company_id' => $request->company_id,
            'name' => $request->name,
            // 'email' => $request->email,
            // 'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'role_id' => 2,
            // 'image_profile' => $image_profile_url,
            'status' => $request->status
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Record update successfully.',
            'data'=> $user
        ], 200);
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::whereRole_id(2)->findOrFail($id);
        $user->delete();
        
        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
}
