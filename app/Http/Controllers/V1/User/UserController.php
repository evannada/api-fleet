<?php

namespace App\Http\Controllers\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function updateStatus(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|integer'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = User::findOrFail($id);
        $user->update(['status' => $request->status]);

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => $user
        ];
        return response()->json($response, 200);
    }
}
