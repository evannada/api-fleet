<?php

namespace App\Http\Controllers\V2;

use App\Models\Armada;
use App\Models\Tiredetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ArmadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'status' => 'nullable',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Armada::query();

        if ($request->has('company_id')) {
            $query = $query->whereCompany_id($request->company_id);
        }

        if ($request->has('status')) {
            $query = $query->whereStatus($request->status);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        $armada = $query->orderBy('id', 'DESC')
        ->with('schedulearmadas')->get();
        
        $response = [
            'status' => 'success',
            'data' => $armada
        ];
        return response()->json($response, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'name' => 'required|string',
            'plate_number' => 'required|string',
            'merk' => 'required|string',
            'series' => 'required|string',
            'color' => 'present|nullable|string',
            'type' => 'required|string',
            'odometer' => 'present|nullable',
            'year' => 'required|string',
            'stnk_number' => 'present|nullable|string',
            'image_armada' => 'present|nullable|string',
            'image_stnk' => 'present|nullable|string',
            'stnk_expired' => 'present|nullable|date_format:Y-m-d',
            'kir_number' => 'present|nullable|string',
            'image_kir' => 'present|nullable|string',
            'kir_expired' => 'present|nullable|date_format:Y-m-d',
            'length' => 'present|nullable|string',
            'width' => 'present|nullable|string',
            'height' => 'present|nullable|string',
            'volume' => 'present|nullable|string',
            'max_weight' => 'present|nullable|string',
            'carrosserie_type' => 'present|nullable|string',
            'carrosserie_condition' => 'present|nullable|string',
            'carrosserie_location' => 'present|nullable|string',
            'insurance' => 'present|nullable|string',
            'insurance_company' => 'present|nullable|string',
            'polis_type' => 'present|nullable|string',
            'no_polis' => 'present|nullable|string',
            'insurance_expired' => 'present|nullable|date_format:Y-m-d',
            'polis_period' => 'present|nullable|date_format:Y-m-d',
            'quality_insurance' => 'present|nullable|string',
            'description_insurance' => 'present|nullable|string',
            'chassis_number' => 'present|nullable|string',
            'machine_number' => 'present|nullable|string',
            'number_tires' => 'present|nullable|integer',
            'status' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_armada)) {
            $image_armada = substr($request->image_armada, strpos($request->image_armada, ",")+1);
            $image_armada_name = time() . '.' . str_random(10) . '.jpg';
            $image_armada_path = public_path() . "/uploads/armada/armada/" . $image_armada_name;
            file_put_contents($image_armada_path, base64_decode($image_armada));
            $image_armada_url = config('global.url'). "/uploads/armada/armada/" . $image_armada_name;
        } else {
            $image_armada_url = config('global.url'). "/uploads/no_image.jpg";
        }


        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_stnk)) {
            $image_stnk = substr($request->image_stnk, strpos($request->image_stnk, ",")+1);
            $image_stnk_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_stnk_path = public_path() . "/uploads/armada/stnk/" . $image_stnk_name;
            file_put_contents($image_stnk_path, base64_decode($image_stnk));
            $image_stnk_url = config('global.url'). "/uploads/armada/stnk/" . $image_stnk_name;
        } else {
            $image_stnk_url = config('global.url'). "/uploads/no_image.jpg";
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_kir)) {
            $image_kir = substr($request->image_kir, strpos($request->image_kir, ",")+1);
            $image_kir_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_kir_path = public_path() . "/uploads/armada/kir/" . $image_kir_name;
            file_put_contents($image_kir_path, base64_decode($image_kir));
            $image_kir_url = config('global.url'). "/uploads/armada/kir/" . $image_kir_name;
        } else {
            $image_kir_url = config('global.url'). "/uploads/no_image.jpg";
        }

        $armada = Armada::create([
            'company_id' => $request->company_id,
            'name' => $request->name,
            'plate_number' => $request->plate_number,
            'merk' => $request->merk,
            'series' => $request->series,
            'color' => $request->color,
            'type' => $request->type,
            'odometer' => $request->odometer,
            'year' => $request->year,
            'stnk_number' => $request->stnk_number,
            'image_armada' => $image_armada_url,
            'image_stnk' => $image_stnk_url,
            'stnk_expired' => $request->stnk_expired,
            'kir_number' => $request->kir_number,
            'image_kir' => $image_kir_url,
            'kir_expired' => $request->kir_expired,
            'length' => $request->length,
            'width' => $request->width,
            'height' => $request->height,
            'volume' => $request->volume,
            'max_weight' => $request->max_weight,
            'carrosserie_type' => $request->carrosserie_type,
            'carrosserie_condition' => $request->carrosserie_condition,
            'carrosserie_location' => $request->carrosserie_location,
            'insurance' => $request->insurance,
            'insurance_company' => $request->insurance_company,
            'polis_type' => $request->polis_type,
            'no_polis' => $request->no_polis,
            'insurance_expired' => $request->insurance_expired,
            'polis_period' => $request->polis_period,
            'quality_insurance' => $request->quality_insurance,
            'description_insurance' => $request->description_insurance,
            'chassis_number' => $request->chassis_number,
            'machine_number' => $request->machine_number,
            'number_tires' => $request->number_tires,
            'status' => $request->status
        ]);
        
        // Tirehistory::insert($armada->tiredetails()->get()->toArray());
        $data = Armada::with('tiredetails')->whereId($armada->id)->get();

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $data[0]
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'nullable|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $armada = Armada::with('tiredetails')->whereCompany_id($request->company_id)->findOrFail($id);
        $tiredetails = Tiredetail::with(['tire', 'tirecondition'])
                    ->whereArmada_id($armada->id)
                    ->orderBy('tire_id', 'ASC')->get();

        $data = array_merge($armada->toArray(), [
            'tiredetails' => $tiredetails->toArray()
            ]);

        $response = [
            'status' => 'success',
            'data' => $data
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'name' => 'required|string',
            'plate_number' => 'required|string',
            'merk' => 'required|string',
            'series' => 'required|string',
            'color' => 'present|nullable|string',
            'type' => 'required|string',
            'odometer' => 'present|nullable',
            'year' => 'required|string',
            'stnk_number' => 'present|nullable|string',
            'image_armada' => 'present|nullable|string',
            'image_stnk' => 'present|nullable|string',
            'stnk_expired' => 'present|nullable|date_format:Y-m-d',
            'kir_number' => 'present|nullable|string',
            'image_kir' => 'present|nullable|string',
            'kir_expired' => 'present|nullable|date_format:Y-m-d',
            'length' => 'present|nullable|string',
            'width' => 'present|nullable|string',
            'height' => 'present|nullable|string',
            'volume' => 'present|nullable|string',
            'max_weight' => 'present|nullable|string',
            'carrosserie_type' => 'present|nullable|string',
            'carrosserie_condition' => 'present|nullable|string',
            'carrosserie_location' => 'present|nullable|string',
            'insurance' => 'present|nullable|string',
            'insurance_company' => 'present|nullable|string',
            'polis_type' => 'present|nullable|string',
            'no_polis' => 'present|nullable|string',
            'insurance_expired' => 'present|nullable|date_format:Y-m-d',
            'polis_period' => 'present|nullable|date_format:Y-m-d',
            'quality_insurance' => 'present|nullable|string',
            'description_insurance' => 'present|nullable|string',
            'chassis_number' => 'present|nullable|string',
            'machine_number' => 'present|nullable|string',
            'number_tires' => 'present|nullable|integer',
            'status' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $armada = Armada::whereCompany_id($request->company_id)->findOrFail($id);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_armada)) {
            $image_armada = substr($request->image_armada, strpos($request->image_armada, ",")+1);
            $image_armada_name = time() . '.' . str_random(10) . '.jpg';
            $image_armada_path = public_path() . "/uploads/armada/armada/" . $image_armada_name;
            file_put_contents($image_armada_path, base64_decode($image_armada));
            $image_armada_url = config('global.url'). "/uploads/armada/armada/" . $image_armada_name;
            $armada->update([
                'image_stnk' => $image_armada_url,
            ]);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_stnk)) {
            $image_stnk = substr($request->image_stnk, strpos($request->image_stnk, ",")+1);
            $image_stnk_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_stnk_path = public_path() . "/uploads/armada/stnk/" . $image_stnk_name;
            file_put_contents($image_stnk_path, base64_decode($image_stnk));
            $image_stnk_url = config('global.url'). "/uploads/armada/stnk/" . $image_stnk_name;
            $armada->update([
                'image_stnk' => $image_stnk_url,
            ]);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_kir)) {
            $image_kir = substr($request->image_kir, strpos($request->image_kir, ",")+1);
            $image_kir_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_kir_path = public_path() . "/uploads/armada/kir/" . $image_kir_name;
            file_put_contents($image_kir_path, base64_decode($image_kir));
            $image_kir_url = config('global.url'). "/uploads/armada/kir/" . $image_kir_name;
            $armada->update([
                'image_kir' => $image_kir_url,
            ]);
        }

        $armada->update([
            'company_id' => $request->company_id,
            'name' => $request->name,
            'plate_number' => $request->plate_number,
            'merk' => $request->merk,
            'series' => $request->series,
            'color' => $request->color,
            'type' => $request->type,
            'odometer' => $request->odometer,
            'year' => $request->year,
            'stnk_number' => $request->stnk_number,
            // 'image_stnk' => $image_stnk_url,
            'stnk_expired' => $request->stnk_expired,
            'kir_number' => $request->kir_number,
            // 'image_kir' => $image_kir_url,
            'kir_expired' => $request->kir_expired,
            'length' => $request->length,
            'width' => $request->width,
            'height' => $request->height,
            'volume' => $request->volume,
            'max_weight' => $request->max_weight,
            'carrosserie_type' => $request->carrosserie_type,
            'carrosserie_condition' => $request->carrosserie_condition,
            'carrosserie_location' => $request->carrosserie_location,
            'insurance' => $request->insurance,
            'insurance_company' => $request->insurance_company,
            'polis_type' => $request->polis_type,
            'no_polis' => $request->no_polis,
            'insurance_expired' => $request->insurance_expired,
            'polis_period' => $request->polis_period,
            'quality_insurance' => $request->quality_insurance,
            'description_insurance' => $request->description_insurance,
            'chassis_number' => $request->chassis_number,
            'machine_number' => $request->machine_number,
            'number_tires' => $request->number_tires,
            'status' => $request->status
        ]);

        $data = Armada::with('tiredetails')->whereId($armada->id)->get();

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => $data[0]
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'nullable|exists:companies,id'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $armada = Armada::whereCompany_id($request->company_id)->findOrFail($id);
        $armada->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }

}
