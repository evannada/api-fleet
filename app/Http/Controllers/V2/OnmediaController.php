<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Models\Onmedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OnmediaController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Onmedia::query();

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        if ($request->has('show')) {
            $query = $query->where('show',$request->show);
        }

        if ($request->has('is_image')) {
            $query = $query->where('is_image',$request->is_image);
        }

        $event = $query->orderBy('id', 'DESC')->get();
        $response = [
            'status' => 'success',
            'data' => $event
        ];
        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required',
            'photo' => 'present|nullable',
            'video_url' => 'present|nullable|string',
            'show' => 'required|integer',
            'is_image' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->photo)) {
            $photo = substr($request->photo, strpos($request->photo, ",") + 1);
            $photo_name = time() . '.' . str_random(10) . '.jpg';
            $photo_path = public_path() . "/uploads/onmedia/" . $photo_name;
            file_put_contents($photo_path, base64_decode($photo));
            $photo_url = config('global.url') . "/uploads/onmedia/" . $photo_name;
        } else {
            $photo_url = config('global.url') . "/uploads/no_image.jpg";
        }

        $onmedia = Onmedia::create([
            'title' => $request->title,
            'description' => $request->description,
            'photo' => $photo_url,
            'video_url' => $request->video_url,
            'show' => $request->show,
            'is_image' => $request->is_image,
        ]);

        $data = Onmedia::whereId($onmedia->id)->get();

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $data[0]
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $data = Onmedia::findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $data
        ];
        return response()->json($response, 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required',
            'photo' => 'present|nullable',
            'video_url' => 'present|nullable|string',
            'show' => 'required|integer',
            'is_image' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $onmedia = Onmedia::findOrFail($id);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->photo)) {
            $photo = substr($request->photo, strpos($request->photo, ",") + 1);
            $photo_name = time() . '.' . str_random(10) . '.jpg';
            $photo_path = public_path() . "/uploads/onmedia/" . $photo_name;
            file_put_contents($photo_path, base64_decode($photo));
            $photo_url = config('global.url') . "/uploads/onmedia/" . $photo_name;
            $onmedia->update([
                'photo' => $photo_url,
            ]);
        }

        $onmedia->update([
            'title' => $request->title,
            'description' => $request->description,
            'photo' => $request->photo_url,
            'video_url' => $request->video_url,
            'show' => $request->show,
            'is_image' => $request->is_image,
        ]);

        $data = Onmedia::whereId($onmedia->id)->get();

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $data[0]
        ];
        return response()->json($response, 200);
    }

    public function destroy($id)
    {
        $onmedia = Onmedia::findOrFail($id);
        $onmedia->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }

    public function get_all(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Onmedia::query();

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        if ($request->has('show')) {
            $query = $query->where('show',$request->show);
        }
        
        if ($request->has('is_image')) {
            $query = $query->where('is_image',$request->is_image);
        }

        $event = $query->orderBy('id', 'DESC')->get();
        $response = [
            'status' => 'success',
            'data' => $event
        ];
        return response()->json($response, 200);
    }
}
