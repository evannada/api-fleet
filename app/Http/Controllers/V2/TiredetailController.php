<?php

namespace App\Http\Controllers\V2;

use App\Models\Tiredetail;
use App\Models\Tirehistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TiredetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $tiredetail = Tiredetail::with(['tire', 'tirecondition'])
                    ->whereCompany_id($request->company_id)
                    ->whereArmada_id($request->armada_id)
                    ->orderBy('tire_id', 'ASC')->get();

        $response = [
            'status' => 'success',
            'data' => $tiredetail
        ];
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
            'tirecondition_id' => 'present|nullable|exists:tireconditions,id',
            'tire_id' => 'required|exists:tires,id', 
            'tire_number' => 'present|nullable|string',
            'image_tire' => 'present|nullable|string',
            'merk' => 'present|nullable|string',
            'production_code' => 'present|nullable|string',
            'serial_number' => 'present|nullable|string',
            'date_installation' => 'present|nullable|date_format:Y-m-d',
            'km' => 'present|nullable|string',
            'thick_tire' => 'present|nullable|string',
            'tire_pressure' => 'present|nullable|string',
            'stamp' => 'present|nullable|string',
            'description'=> 'present|nullable|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_tire)) {
            $image_tire = substr($request->image_tire, strpos($request->image_tire, ",")+1);
            $image_tire_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_tire_path = public_path() . "/uploads/armada/tire/" . $image_tire_name;
            file_put_contents($image_tire_path, base64_decode($image_tire));
            $image_tire_url = config('global.url'). "/uploads/armada/tire/" . $image_tire_name;
        } else {
            $image_tire_url = config('global.url'). "/uploads/no_image.jpg";
        }

        $tiredetail = Tiredetail::create([
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'tirecondition_id'=> $request->tirecondition_id,
            'tire_id'=> $request->tirecondition_id,
            'tire_number'=> $request->tirecondition_id,
            'image_tire'=> $image_tire_url,
            'merk'=> $request->tirecondition_id,
            'production_code'=> $request->tirecondition_id,
            'serial_number'=> $request->tirecondition_id,
            'date_installation'=> $request->tirecondition_id,
            'km'=> $request->tirecondition_id,
            'thick_tire'=> $request->tirecondition_id,
            'tire_pressure'=> $request->tirecondition_id,
            'stamp'=> $request->tirecondition_id,
            'description'=> $request->tirecondition_id,
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $tiredetail
        ];
        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $tiredetail = Tiredetail::with(['tire', 'tirecondition'])
                    ->whereCompany_id($request->company_id)
                    ->whereArmada_id($request->armada_id)
                    ->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $tiredetail
        ];
        return response()->json($response, 200);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
            'tirecondition_id' => 'present|nullable|exists:tireconditions,id',
            'tire_id' => 'required|exists:tires,id', 
            'tire_number' => 'present|nullable|string',
            'image_tire' => 'present|nullable|string',
            'merk' => 'present|nullable|string',
            'production_code' => 'present|nullable|string',
            'serial_number' => 'present|nullable|string',
            'date_installation' => 'present|nullable|date_format:Y-m-d',
            'km' => 'present|nullable|string',
            'thick_tire' => 'present|nullable|string',
            'tire_pressure' => 'present|nullable|string',
            'stamp' => 'present|nullable|string',
            'description'=> 'present|nullable|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $tiredetail = Tiredetail::whereCompany_id($request->company_id)
                    ->whereArmada_id($request->armada_id)
                    ->findOrFail($id);

        Tirehistory::create($tiredetail->toArray());
        $tiredetail->forceDelete();

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_tire)) {
            $image_tire = substr($request->image_tire, strpos($request->image_tire, ",")+1);
            $image_tire_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_tire_path = public_path() . "/uploads/armada/tire/" . $image_tire_name;
            file_put_contents($image_tire_path, base64_decode($image_tire));
            $image_tire_url = config('global.url'). "/uploads/armada/tire/" . $image_tire_name;
        } else {
            $tirehistory = Tirehistory::whereCompany_id($request->company_id)
                ->whereTire_id($request->tire_id)->latest()->first();
            $image_tire_url = $tirehistory->image_tire;
        }

        $tiredetail = Tiredetail::create([
            'company_id' => $request->company_id,
            'armada_id' => $request->armada_id,
            'tirecondition_id'=> $request->tirecondition_id,
            'tire_id'=> $request->tire_id,
            'tire_number'=> $request->tire_number,
            'image_tire'=> $image_tire_url,
            'merk'=> $request->merk,
            'production_code'=> $request->production_code,
            'serial_number'=> $request->serial_number,
            'date_installation'=> $request->date_installation,
            'km'=> $request->km,
            'thick_tire'=> $request->thick_tire,
            'tire_pressure'=> $request->tire_pressure,
            'stamp'=> $request->stamp,
            'description'=> $request->description,
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => $tiredetail
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'armada_id' => 'required|exists:armadas,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $tiredetail = Tiredetail::whereCompany_id($request->company_id)
                    ->whereArmada_id($request->armada_id)
                    ->findOrFail($id);
        $tiredetail->delete();
        
        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);

    }
}
