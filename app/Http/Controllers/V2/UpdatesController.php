<?php

namespace App\Http\Controllers\V2;

use App\Http\Controllers\Controller;
use App\Models\Updates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UpdatesController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Updates::query();

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        if ($request->has('show')) {
            $query = $query->where('show',$request->show);
        }

        if ($request->has('is_image')) {
            $query = $query->where('is_image',$request->is_image);
        }

        $event = $query->orderBy('id', 'DESC')->get();
        $response = [
            'status' => 'success',
            'data' => $event
        ];
        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required',
            'photo' => 'present|nullable',
            'video_url' => 'present|nullable|string',
            'show' => 'required|integer',
            'is_image' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->photo)) {
            $photo = substr($request->photo, strpos($request->photo, ",") + 1);
            $photo_name = time() . '.' . str_random(10) . '.jpg';
            $photo_path = public_path() . "/uploads/maintenance/" . $photo_name;
            file_put_contents($photo_path, base64_decode($photo));
            $photo_url = config('global.url') . "/uploads/maintenance/" . $photo_name;
        } else {
            $photo_url = config('global.url') . "/uploads/no_image.jpg";
        }
        
        $updates = Updates::create([
            'title' => $request->title,
            'description' => $request->description,
            'photo' => $photo_url,
            'video_url' => $request->video_url,
            'show' => $request->show,
            'is_image' => $request->is_image,
        ]);

        $data = Updates::whereId($updates->id)->get();

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $data[0]
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $data = Updates::findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $data
        ];
        return response()->json($response, 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required',
            'photo' => 'present|nullable',
            'video_url' => 'present|nullable|string',
            'show' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $updates = Updates::findOrFail($id);

        // if(!empty($request->photo)){
        if (preg_match('/^data:image\/(\w+);base64,/', $request->photo)) {
            $photo = substr($request->photo, strpos($request->photo, ",") + 1);
            $photo_name = time() . '.' . str_random(10) . '.jpg';
            $photo_path = public_path() . "/uploads/maintenance/" . $photo_name;
            file_put_contents($photo_path, base64_decode($photo));
            $photo = config('global.url') . "/uploads/maintenance/" . $photo_name;
            $updates->update([
                'photo' => $photo,
            ]);
        }
        // }else{
        //     $photo=null;
        // }

         
        if(!empty($photo)){
        $updates->update([
            'title' => $request->title,
            'description' => $request->description,
            'photo' => $photo,
            'video_url' => $request->video_url,
            'show' => $request->show,
            'is_image' => $request->is_image,
        ]);
        }else{
            $updates->update([
                'title' => $request->title,
                'description' => $request->description,
                'video_url' => $request->video_url,
                'show' => $request->show,
                'is_image' => $request->is_image,
            ]); 
        }

        // return dd($updates);

        $data = Updates::whereId($updates->id)->get();

        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' => $data[0]
        ];
        return response()->json($response, 200);
    }

    public function destroy($id)
    {
        $updates = Updates::findOrFail($id);
        $updates->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }

    public function get_all(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Updates::query();

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }

        if ($request->has('show')) {
            $query = $query->where('show',$request->show);
        }
        
        if ($request->has('is_image')) {
            $query = $query->where('is_image',$request->is_image);
        }

        $event = $query->orderBy('id', 'DESC')->get();
        $response = [
            'status' => 'success',
            'data' => $event
        ];
        return response()->json($response, 200);
    }
}
