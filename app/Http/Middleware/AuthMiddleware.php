<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Access;
class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $Access=Access::where('name',$_SERVER['PHP_AUTH_USER'])->where('key_access',$_SERVER['PHP_AUTH_PW'])->first();
        if(!empty($Access)){
        $AUTH_USER = $Access->name;
        $AUTH_PASS = $Access->key_access;
        }else{
            return response()->json([
                'status' => 'error',
                'status_code'=>'401',
                'message' => 'Transaction cannot be authorized with the current client/server key.'
            ], 401); 
        }
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $is_not_authenticated = (
            !$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
        );
        // if(!(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']))){
        // $Access=Access::where('name',$_SERVER['PHP_AUTH_USER'])->where('key_access',$_SERVER['PHP_AUTH_PW'])->firstOrFail();
        // }else{
        // return response()->json([
        //         'status' => 'error',
        //         'status_code'=>'401',
        //         'message' => 'Transaction cannot be authorized with the current client/server key.'
        //     ], 401);
        // }
        // header('Cache-Control: no-cache, must-revalidate, max-age=0');
        // $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        // $is_not_authenticated = (
        //     !$has_supplied_credentials ||
        //     $_SERVER['PHP_AUTH_USER'] != $Access->name ||
        //     $_SERVER['PHP_AUTH_PW']   != $Access->key_access
        // );
        if ($is_not_authenticated) {
           return response()->json([
                'status' => 'error',
                'status_code'=>'401',
                'message' => 'Transaction cannot be authorized with the current client/server key.'
            ], 401);
        }
        return $next($request);
    }
}