<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationPaymentAddUser extends Mailable
{
    use Queueable, SerializesModels;

    protected $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('notification@digkontrol.com')
            ->markdown('emails.notification-payment-add-user')
            ->with([
                'to' => $this->details['to'],
                'owner_name' => $this->details['owner_name'],
                'message' => $this->details['message'],
                'address' => $this->details['address'],
                'phone' => $this->details['phone'],
                'npwp' => $this->details['npwp'],
                'no_payment' => $this->details['no_payment'],
                'url_order_payment' => $this->details['url_order_payment'],
            ]);
    }
}
