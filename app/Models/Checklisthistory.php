<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class Checklisthistory extends Model
{
    use AuditableTrait;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'armada_id', 'maintenance_id', 
        'checklistvehicle_id', 'detail', 'status', 
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function armada()
    {
        return $this->belongsTo(Armada::class)->withTrashed();
    }

    public function maintenance()
    {
        return $this->belongsTo(Maintenance::class);
    }

    public function checklistvehicle()
    {
        return $this->belongsTo(Checklistvehicle::class)->withTrashed();
    }

}
