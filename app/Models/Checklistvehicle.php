<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class Checklistvehicle extends Model
{
    use AuditableTrait;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'name',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function checklistdetails()
    {
        return $this->hasMany(Checklistdetail::class);
    }

    public function checklisthistories()
    {
        return $this->hasMany(Checklisthistory::class);
    }

    public function armadareminders()
    {
        return $this->hasMany(Armadareminder::class);
    }

}
