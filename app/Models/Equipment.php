<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;
use App\User;

class Equipment extends Model
{
  use AuditableTrait;
  use SoftDeletes;
  
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'equipment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'user_id', 'simtype_id', 'sim_expired',
        'no_sim', 'no_ktp', 'image_sim', 'image_ktp',
    ];

    public function company()
    {
      return $this->belongsTo(Company::class);
    }

    public function user()
    {
      return $this->belongsTo(User::class)->withTrashed();
    }

    public function simtype()
    {
      return $this->belongsTo(Simtype::class);
    }

}
