<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;
use App\User;

class Maintenance extends Model
{
    use AuditableTrait;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'armada_id', 'maintenancetype_id', 'reporter', 'no_maintenance',
        'odometer', 'date_maintenance', 'expected_complete', 'chronologic', 'description_accident',
        'location_accident', 'date_accident', 'image_1', 'image_2', 'vendor',
        'performance_vendor', 'cost', 'note', 'actual_completion_date', 'status',
    ];

    public function checklistdetails()
    {
        return $this->hasMany(Checklistdetail::class)->withTrashed();
    }

    public function checklisthistories()
    {
        return $this->hasMany(Checklisthistory::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function armada()
    {
        return $this->belongsTo(Armada::class)->withTrashed();
    }

    public function maintenancetype()
    {
        return $this->belongsTo(Maintenancetype::class)->withTrashed();
    }

    


    
}
