<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class Odometerhistory extends Model
{
    use AuditableTrait;
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'armada_id', 'date', 'km', 'liter', 
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function armada()
    {
        return $this->belongsTo(Armada::class)->withTrashed();
    }

}
