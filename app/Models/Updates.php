<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Updates extends Model
{
	protected $table='updates';
    protected $fillable = [
        'title', 'description', 'photo', 'video_url','show' ,'is_image'
    ];
}
