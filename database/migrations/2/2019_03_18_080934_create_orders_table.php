<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->unsignedBigInteger('armada_id');
            $table->foreign('armada_id')->references('id')->on('armadas')->onDelete('cascade');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->string('no_order')->unique()->nullable();
            $table->date('order_date')->nullable();
            $table->dateTime('departure_date')->nullable();
            $table->dateTime('arrival_date')->nullable();
            $table->string('item_type')->nullable();//jenis barang
            $table->string('item_weight')->nullable();
            $table->string('item_qty')->nullable();
            $table->longText('item_detail')->nullable();
            $table->text('image_item')->nullable();
            $table->double('bruto', 50, 2)->default(0)->nullable();
            $table->double('disc', 50, 2)->default(0)->nullable();
            $table->double('disc_value', 50, 2)->default(0)->nullable();
            $table->double('ppn', 50, 2)->default(0)->nullable();
            $table->double('ppn_value', 50, 2)->default(0)->nullable();
            $table->double('pph', 50, 2)->default(0)->nullable();
            $table->double('pph_value', 50, 2)->default(0)->nullable();
            $table->double('netto', 50, 2)->default(0)->nullable();
            $table->double('payment', 50, 2)->default(0)->nullable();
            $table->double('outstanding', 50, 2)->default(0)->nullable();
            $table->date('due_date')->nullable();
            $table->double('total_budgetvalue', 50, 2)->default(0)->nullable();
            $table->dateTime('driver_departure_date')->nullable();
            $table->string('driver_actual_hours')->nullable();
            $table->string('driver_actual_minutes')->nullable();
            $table->longText('driver_note')->nullable();
            $table->string('no_sj')->unique()->nullable();
            $table->date('sj_date')->nullable();
            $table->string('no_invoice')->unique()->nullable();
            $table->date('invoice_date')->nullable();
            $table->integer('disciplinerating')->nullable();
            $table->integer('servicerating')->nullable();
            $table->integer('safetyrating')->nullable();
            $table->longText('commentrating')->nullable();
            $table->integer('status_order')->default(0);
            $table->auditable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
