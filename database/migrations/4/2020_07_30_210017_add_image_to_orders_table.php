<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->text('image_item2')->after('image_item')->nullable();
            $table->text('image_item3')->after('image_item2')->nullable();
            $table->text('file_signed')->after('image_item3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('image_item2');
            $table->dropColumn('image_item3');
            $table->dropColumn('file_signed');
        });
    }
}
