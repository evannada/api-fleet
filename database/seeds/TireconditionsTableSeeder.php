<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TireconditionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tireconditions')->insert([
            'name' => 'Orisinil'
        ]);

        DB::table('tireconditions')->insert([
            'name' => 'Vulkanisir'
        ]);

        DB::table('tireconditions')->insert([
            'name' => 'Rotasi'
        ]);
        
    }
}
