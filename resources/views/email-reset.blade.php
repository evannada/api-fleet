<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Reset Password</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">

  <!-- CSS Libraries -->
<link rel="stylesheet" href="{{ asset('assets/modules/sweetalert2/sweetalert2.min.css') }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

  <style>
    .btn-danger, .btn-danger.disabled {
        box-shadow: 0 2px 6px #acb5f6;
        background-color: #Da2827;
        border-color: #Da2827;
    }
  </style>
</head>
<body>
    <div id="app">
        <section class="section">
          <div class="container mt-5">
            <div class="row">
              <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand">
                  <img src="{{URL::to('icons/logo.png')}}" alt="Logo" width="140px">
              </div>
    
                <div class="card card-danger">
                  <div class="card-header"><h4>Reset Password</h4></div>
                  <div class="card-body">
                    <form id="form" method="POST">
                          @csrf
                          @method('POST')
                          <input type="hidden" name="token" value="{{$token}}">

                          <div class="form-group">
                              <label for="password">New Password</label>
                              <input id="password" type="password" class="form-control" name="password" minlength="8" required>
                          </div>
                  
                          <div class="form-group">
                              <label for="password_confirmation">Password Confirmation</label>
                              <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" minlength="8" required>
                              @if ($message = Session::get('error'))
                              <span>
                                <strong>{{ $message }}</strong>
                              </span>
                              @endif
                          </div>
                  
                          <div class="form-group">
                              <button type="submit" class="btn btn-danger form-control">
                                  Reset Password
                              </button>
                          </div>
                      </form>
                  </div>
                </div>
                <div class="simple-footer">
                  Copyright &copy; 2019 PT Fitlogindo Mega Tama All rights reserved.
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
<!-- General JS Scripts -->
<script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
<script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('assets/modules/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/stisla.js') }}"></script>

<!-- JS Libraies -->
<script src="{{ asset('assets/modules/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- Template JS File -->
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script src="{{ asset('assets/js/custom.js') }}"></script>

<script>
$(function(){
    $('#form').on('submit', function (e) {
        if (!e.isDefaultPrevented()){
            $.ajax({
              url: "{{ route('reset.password') }}",
                type : "POST",
                data : $('#form').serialize(),
                success : function(response) {
                  $('#form')[0].reset();
                    swal({
                        title: 'Success!',
                        text: response.message,
                        type: 'success',
                        timer: '1500'
                    })
                },
                error : function(response){
                    swal({
                        title: 'Opps...',
                        text: response.responseText,
                        type: 'error',
                        timer: '2000'
                    })
                }
            });
            return false;
        }
    });
});
</script>
</body>
</html>