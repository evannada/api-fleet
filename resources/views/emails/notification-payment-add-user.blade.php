@component('mail::message')
# Hi, {{$owner_name}}

## Mohon untuk membayar dahulu invoice {{$message}} untuk menambah jumlah user.

@component('mail::button', ['url' => $url_order_payment])
Klik disini untuk ke Halaman Pembayaran
@endcomponent



Salam Kontrol, <br>
Tim {{ config('app.name') }}
@endcomponent