@component('mail::message')
# Hi, {{$name}}

Terimakasih Telah mendaftar <b>Digkontrol</b> <br>
Silahkan cek email {{$email}} untuk verifikasi admin.

Thanks, <br>
Tim {{ config('app.name') }}
@endcomponent
