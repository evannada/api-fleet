@component('mail::message')
# Hi, {{ $username }}

Kami menerima permintaan untuk mengubah kata sandi,
Jika Anda tidak membuat permintaan ini silahkan abaikan
pesan ini,
Anda bisa menggunakan link dibawah ini untuk mengubah
Kata Sandi 

@component('mail::button', ['url' => $url_reset])
Klik disini untuk Mengubah Kata Sandi
@endcomponent

Atau copy and paste link dibawah ini di browser Anda <br>
@php
$subject = $url_reset;
$search = 'amp;signature';
$trimmed = str_replace($search, 'signature', $subject);
echo $trimmed;
@endphp <br>

Thanks, <br>
Tim {{ config('app.name') }}
@endcomponent
