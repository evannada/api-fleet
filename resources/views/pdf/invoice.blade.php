<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">  

</head>
<body>
  @foreach ($company as $com)
      <img class="img-dasboard" style="width:100%;height:15%;margin:auto" src="{{ $com->file_invoice }}"></div>
  @endforeach
  <br/>
  <br/>
  <h2 style="padding-top:4cm;"><b>Invoice</b></h2>
  <hr style="border-bottom:1px solid black;">
  <table width="100%">
    <tr>
      <td>
        <strong>Menagih Ke :</strong><br>
        {{$order->customer['name']}}
      </td>
      <td class="text-right">
        <strong>Tanggal Order :</strong><br>
        {{date('d F Y', strtotime($order->order_date))}}
        </td>
    </tr>
    <tr>
        <td>
          <strong>No. Invoice :</strong><br>
          {{$order->no_invoice}} <br>
          <strong>Tanggal Invoice :</strong><br>
          {{date('d F Y', strtotime($order->invoice_date))}}
        </td>
        <td class="text-right">
          <strong>Tanggal Jatuh Tempo :</strong><br>
          {{date('d F Y', strtotime($order->due_date))}}
        </td>
      </tr>
    </table>
    <hr style="border-bottom:1px solid black;">
    <h5><strong>Keterangan Pemesanan</strong></h5>
    <table width="100%">
      <tr>
        <td>
          <strong>Tujuan</strong>
        </td>
      </tr>
      <tr>
          <td>
            Dari :<br>
            <p>{{$order->routes[0]['address']}}</p> 
          </td>
          <td class="text-right">
            Ke :<br>
            <p>{{$order->routes[1]['address']}}<br>
              Nama PIC : {{$order->routes[1]['pic_name']}}<br>
              Kontak PIC : {{$order->routes[1]['pic_contact']}}<br>
            </p> 
          </td>
      </tr>
      </table>
      <hr style="border-bottom:1px solid black;">
      <h5><strong>Biaya Perjalanan</strong></h5><br>
      <table width="100%" border="1">
        <tr>
          <td colspan="2"><strong>Keterangan</strong></td>
        </tr>
        @foreach ($order->orderdetailcosts as $orderdetailcost)
          @if ($orderdetailcost['value'] != 0)
            <tr>
              <td>{{$orderdetailcost['cost']['name']}}</td>
              <td class="text-right">{{ number_format($orderdetailcost['value']) }}</td>
            </tr>
          @endif
        @endforeach
        <tr>
          <td><strong>Subtotal</strong></td>
          <td class="text-right">{{ number_format($order->bruto) }}</td>
        </tr>
        <tr>
          <td>Discount {{$order->disc}}%</td>
        <td class="text-right">{{ number_format($order->disc_value) }}</td>
        </tr>
        <tr>
          <td>PPN {{$order->ppn}}%</td>
          <td class="text-right">{{ number_format($order->ppn_value) }}</td>
        </tr>
        <tr>
          <td>PPH {{$order->pph}}%</td>
          <td class="text-right">{{ number_format($order->pph_value) }}</td>
        </tr>
        <tr>
          <td><strong>Total</strong></td>
          <td class="text-right">{{ number_format($order->netto) }}</td>
        </tr>
        </table>
        <br>
        <table width="100%" border="1">
          <tr>
            <td colspan="2"><strong>Tagihan</strong></td>
          </tr>
          <tr>
            <td>Total</td>
            <td class="text-right">{{ number_format($order->netto) }}</td>
          </tr>
          <tr>
            <td>Dibayarkan</td>
            <td class="text-right">{{ number_format($order->payment) }}</td>
          </tr>
          <tr>
            <td><strong>Tertagih</strong></td>
            <td class="text-right">{{ number_format($order->outstanding) }}</td>
          </tr>
          </table>
          <table width="100%">
            <tr>
                <td>Catatan :<br>
                <p>
                Pembayaran dilakukan ke bank ______________ dengan no. rekening ________________ <br>
                atas nama ________________</p>
            </tr>
          </table><br>
          <table width="100%">
              <tr>
                <td class="text-right">
                  <b>Ttd.,</b>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <br>
                  <br>
                  <br>
                  <br>
                  ______________________
                </td>
              </tr>
            </table>
        


  
</body>
</html>


<!------ Include the above in your HEAD tag ---------->

