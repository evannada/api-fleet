<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">  

</head>
<body>

  @foreach ($company as $com)
    <img class="img-dasboard" style="width:100%;height:15%;margin:auto;" src="{{ $com->file_surat_jalan }}">  
  @endforeach

  <br/>
  <br/>
  <h2 style="padding-top:4cm;"><b>Proof Of Delivery</b></h2>
  <hr style="border-bottom:1px solid black;">

  <div style=" position: relative; left: 20px; top: 0;">

  <table width="100%">
    <tr>
        <td>
          <strong>No. Surat Jalan :</strong><br>
          {{$order->no_sj}}
        </td>
        <td class="text-right">
          <strong>Tanggal Surat Jalan :</strong><br>
          {{date('d F Y', strtotime($order->sj_date))}}
        </td>
      </tr>
    </table>
    <hr style="border-bottom:0.5px solid black;">
    <h5><strong>Keterangan Pemesanan</strong></h5>
    <table width="100%">
      <tr>
        <td>
          <strong>Tujuan</strong>
        </td>
      </tr>
      <tr>
          <td>
            Dari :<br>
            <p>{{$order->routes[0]['address']}}</p> 
          </td>
          <td class="text-right">
            Ke :<br>
            <p>{{$order->routes[1]['address']}}<br>
              Nama PIC : {{$order->routes[1]['pic_name']}}<br>
              Kontak PIC : {{$order->routes[1]['pic_contact']}}<br>
            </p> 
          </td>
      </tr>
      </table>
      <strong>Detail Barang</strong><br><br>
      <table width="100%" border="1">
          <tr>
              <td>Jenis</td>
              <td>Kode</td>
              <td>Nama</td>
              <td>Satuan</td>
              <td>Berat</td>
              <td>Dimensi</td>
              <td>Jumlah</td>
          </tr>
          @foreach($orderdetail as $row)
          <tr>
              <td class="text-left">{{$row->item_type}}</td>
              <td class="text-left">{{$row->item_code}}</td>
              <td class="text-left">{{$row->item_name}}</td>
              <td class="text-left">{{$row->unit}}</td>
              <td class="text-right">{{$row->weight}}</td>
              <td class="text-left">{{$row->dimensi}}</td>
              <td class="text-right">{{$row->qty}}</td>
          </tr>
          @endforeach
          </table>
          <br>
          <table width="100%" border="1">
            <tr>
                <td>Catatan Pengemudi :<br>
                <p>{{$order->driver_note}}</p>
            </tr>
          </table>
          <br>
          <img class="img-dasboard" style="width:25%;height:30%;float:right;" src="{{ $order->file_signed }}">
          <table width="100%">
              <tr>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <b>Ttd. Pengirim,</b>
                  <br>
                  <br>
                  <br>
                  <br>
                   <br>
                  ______________________
                </td>
                <td align="center">
                    
                  <b>Ttd. Penerima,</b>
                  <br>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <center><img src="{{$order->file_signed}}"  style="width:100px;margin-bottom:0px;"></center>
                  <br> ______________________
                </td>
              </tr>
            </table>
</body>
</html>


<!------ Include the above in your HEAD tag ---------->

